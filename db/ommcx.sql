-- phpMyAdmin SQL Dump
-- version 2.10.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Apr 22, 2009 at 10:36 AM
-- Server version: 5.0.67
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `ommcx`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bankmaster`
-- 

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL auto_increment,
  `bankName` varchar(60) character set utf8 NOT NULL default '',
  `phone1` varchar(12) character set utf8 NOT NULL default '',
  `phone2` varchar(12) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`bankId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `bankmaster`
-- 

INSERT INTO `bankmaster` (`bankId`, `bankName`, `phone1`, `phone2`) VALUES 
(1, 'Bill', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `bhavcopy`
-- 

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL auto_increment,
  `exchange` varchar(30) NOT NULL default '',
  `bhavcopyDate` date NOT NULL default '0000-00-00',
  `sessionId` varchar(15) NOT NULL default '',
  `marketType` varchar(15) NOT NULL default '',
  `instrumentId` int(10) NOT NULL default '0',
  `instrumentName` varchar(15) NOT NULL default '',
  `scriptCode` int(10) NOT NULL default '0',
  `contractCode` varchar(20) NOT NULL default '',
  `scriptGroup` varchar(5) NOT NULL default '',
  `scriptType` varchar(5) NOT NULL default '',
  `expiryDate` date NOT NULL default '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL default '',
  `strikePrice` float NOT NULL default '0',
  `optionType` varchar(4) NOT NULL default '',
  `previousClosePrice` float NOT NULL default '0',
  `openPrice` float NOT NULL default '0',
  `highPrice` float NOT NULL default '0',
  `lowPrice` float NOT NULL default '0',
  `closePrice` float NOT NULL default '0',
  `totalQtyTrade` int(10) NOT NULL default '0',
  `totalValueTrade` double NOT NULL default '0',
  `lifeHigh` float NOT NULL default '0',
  `lifeLow` float NOT NULL default '0',
  `quoteUnits` varchar(10) NOT NULL default '',
  `settlementPrice` float NOT NULL default '0',
  `noOfTrades` int(6) NOT NULL default '0',
  `openInterest` double NOT NULL default '0',
  `avgTradePrice` float NOT NULL default '0',
  `tdcl` float NOT NULL default '0',
  `lstTradePrice` float NOT NULL default '0',
  `remarks` text NOT NULL,
  PRIMARY KEY  (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bhavcopy`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) NOT NULL default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) NOT NULL default '',
  `dwAmount` float NOT NULL default '0',
  `plStatus` char(2) NOT NULL default '',
  `plAmount` float NOT NULL default '0',
  `transactionDate` date default NULL,
  `transType` varchar(20) default NULL,
  `transMode` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `passwd` varchar(30) character set utf8 default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `oneSideBrok` float default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `remiser` int(6) default '0',
  `remiserBrok` float default '0',
  `remiserBrokIn` tinyint(1) default '1',
  `clientBroker` int(2) NOT NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `client`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(10) default NULL,
  `exchange` varchar(30) character set utf8 NOT NULL,
  `oneSideBrok` float default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientexchange`
-- 

CREATE TABLE `clientexchange` (
  `clientexchangeId` int(6) NOT NULL auto_increment,
  `clientId` int(6) NOT NULL,
  `exchange` varchar(50) NOT NULL,
  `brok` int(50) NOT NULL,
  PRIMARY KEY  (`clientexchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientexchange`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `exchange`
-- 

CREATE TABLE `exchange` (
  `exchangeId` int(6) unsigned NOT NULL auto_increment,
  `exchange` varchar(20) character set utf8 NOT NULL,
  `multiply` tinyint(1) NOT NULL default '0',
  `profitBankRate` float default NULL,
  `lossBankRate` float default NULL,
  PRIMARY KEY  (`exchangeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `exchange`
-- 

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES 
(1, 'MCX', 0, 1, 1),
(2, 'F_O', 0, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `expensemaster`
-- 

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL auto_increment,
  `expenseName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`expensemasterId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `expensemaster`
-- 

INSERT INTO `expensemaster` (`expensemasterId`, `expenseName`) VALUES 
(1, 'Light'),
(3, 'Petrol2');

-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(20) character set utf8 NOT NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=300 ;

-- 
-- Dumping data for table `expiry`
-- 

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES 
(280, 'GOLD', '19MAR2009', 'MCX'),
(281, 'CRUDEOIL', '19MAR2009', 'MCX'),
(282, 'COPPER', '19MAR2009', 'MCX'),
(283, 'GOLDM', '19MAR2009', 'MCX'),
(284, 'NATURALGAS', '19MAR2009', 'MCX'),
(285, 'ZINC', '19MAR2009', 'MCX'),
(286, 'NICKEL', '19MAR2009', 'MCX'),
(287, 'LEAD', '19MAR2009', 'MCX'),
(288, 'NIFTY', '28MAR2009', 'F_O'),
(289, 'REL', '28MAR2009', 'F_O'),
(290, 'SILVER', '21MAR2009', 'MCX'),
(291, 'SILVER', '28MAR2009', 'MCX'),
(292, 'GOLD', '28MAR2009', 'MCX'),
(295, 'GOLDM', '28MAR2009', 'MCX'),
(296, 'NATURALGAS', '28MAR2009', 'MCX'),
(297, 'ZINC', '28MAR2009', 'MCX'),
(298, 'NICKEL', '28MAR2009', 'MCX'),
(299, 'LEAD', '28MAR2009', 'MCX');

-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `general`
-- 

INSERT INTO `general` (`generalId`, `filePath`, `fileName`) VALUES 
(1, 'bhavcopies', 'MS20081227.csv');

-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` float default NULL,
  `brok2` float default NULL,
  `oneSideBrok` float default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchangeId` int(6) unsigned NOT NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `multiply` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchangeId`, `exchange`, `multiply`) VALUES 
('SILVER', 'SILVER', 'SILVER', 150, 150, 150, 30, 1, 1, 15500, 30000, NULL, 1, 'MCX', 0),
('GOLD', 'GOLD', 'GOLD', 300, 200, 500, 100, 10, 1, 7000, 15400, NULL, 1, 'MCX', 0),
('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', 0, 0, 500, 100, 1, 1, 1400, 6500, NULL, 1, 'MCX', 0),
('COPPER', 'COPPER', 'COPPER', 0, 0, 500, 1000, 1, 1, 130, 420, NULL, 1, 'MCX', 0),
('GOLDM', 'GOLDM', 'GOLDM', 100, 100, 100, 10, 1, 1, 0, 0, NULL, 1, 'MCX', 0),
('NATURALGAS', 'NATURALGAS', 'NATURALGAS', 100, 100, 0, 1250, 1, 1, 0, 0, NULL, 1, 'MCX', 0),
('ZINC', 'ZINC', 'ZINC', 100, 100, 0, 5000, 1, 1, 30, 120, NULL, 1, 'MCX', 0),
('NICKEL', 'NICKEL', 'NICKEL', 100, 100, 0, 250, 1, 1, 600, 1200, NULL, 1, 'MCX', 0),
('LEAD', 'LEAD', 'LEAD', 0, 0, 1000, 5000, 1, 1, 0, 0, NULL, 1, 'MCX', 0),
('NIFTY', 'NIFTY', 'NIFTY', 0, 0, 10, 50, 1, 1, 0, 0, NULL, 2, 'F_O', 0),
('REL', 'REL', 'REL', 0, 0, 1, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `menu`
-- 

CREATE TABLE `menu` (
  `menuId` int(10) unsigned NOT NULL auto_increment,
  `fileToOpen` varchar(200) character set utf8 NOT NULL,
  `title` varchar(55) character set utf8 NOT NULL,
  `displayToAdmin` tinyint(1) unsigned NOT NULL default '0',
  `displayToOperator` tinyint(1) unsigned NOT NULL default '0',
  `displayToClient` tinyint(1) unsigned NOT NULL default '0',
  `newWindow` tinyint(1) unsigned NOT NULL default '0',
  `newWindowName` char(20) NOT NULL,
  `newWindowPerameter` text character set utf8 NOT NULL,
  PRIMARY KEY  (`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `menu`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `newexpmaster`
-- 

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL auto_increment,
  `newExpName` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `newexpmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `clientId2` int(6) default NULL,
  `firstName2` varchar(35) character set utf8 default NULL,
  `middleName2` varchar(35) character set utf8 default NULL,
  `lastName2` varchar(35) character set utf8 default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `price2` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `refOrderId` int(11) default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `otherexp`
-- 

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL auto_increment,
  `otherExpName` varchar(50) character set utf8 default NULL,
  `otherExpDate` date default NULL,
  `otherExpAmount` float default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherExpMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `otherexp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `partybrokerage`
-- 

CREATE TABLE `partybrokerage` (
  `partybrokerageId` int(11) NOT NULL auto_increment,
  `partyId` int(11) default NULL,
  `brokerageDate` date default NULL,
  `brokerage` float NOT NULL,
  PRIMARY KEY  (`partybrokerageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `partybrokerage`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `settingsKey` varchar(30) character set utf8 default NULL,
  `value` varchar(60) default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `settings`
-- 

INSERT INTO `settings` (`settingsId`, `settingsKey`, `value`) VALUES 
(1, 'uploadFileWorks', '1'),
(2, 'expiryDisplay', 'monthOnly'),
(3, 'profitBankRate', '44'),
(4, 'lossBankRate', '44.50'),
(5, 'clientFieldInTxt', 'ownClient'),
(6, 'clientFieldInTxt', 'userRemarks'),
(7, 'takeTimeEntry', '0'),
(8, 'takeTradeNote', '0'),
(9, 'qtyInLots', '0'),
(10, 'useItemPriceRange', '1'),
(11, 'odinTxtFilePath', 'C:\\ODIN\\DIET\\OnLineBackup\\MCX\\Trades'),
(12, 'billWithLedger', '1');

-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- 
-- Dumping data for table `standing`
-- 

INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`, `exchange`) VALUES 
(1, '2009-04-02', '2009-04-04', 'NIFTY28MAR2009', 15, 'F_O'),
(2, '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', 1, 'F_O'),
(3, '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', 1, 'F_O'),
(4, '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', 1, 'F_O'),
(5, '2009-04-01', '2009-04-01', 'REL28MAR2009', 2, 'F_O'),
(6, '2009-04-01', '2009-04-01', 'COPPER19MAR2009', 3, 'MCX'),
(7, '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', 55, 'F_O'),
(8, '2009-04-01', '2009-04-01', 'REL28MAR2009', 55, 'F_O'),
(9, '2009-04-01', '2009-04-01', 'NIFTY28MAR2009', 3600, 'F_O'),
(10, '2009-04-01', '2009-04-01', 'REL28MAR2009', 12500, 'F_O'),
(11, '2009-04-01', '2009-04-01', 'COPPER19MAR2009', 1500, 'MCX'),
(12, '2009-04-01', '2009-04-01', 'CRUDEOIL19MAR2009', 252, 'MCX'),
(13, '2009-04-01', '2009-04-01', 'GOLD19MAR2009', 45885, 'MCX'),
(14, '2009-04-01', '2009-04-01', 'GOLD28MAR2009', 3500, 'MCX'),
(15, '2009-04-01', '2009-04-01', 'GOLDM19MAR2009', 45022, 'MCX'),
(16, '2009-04-01', '2009-04-01', 'GOLDM28MAR2009', 1500, 'MCX'),
(17, '2009-04-01', '2009-04-01', 'LEAD19MAR2009', 544, 'MCX'),
(18, '2009-04-01', '2009-04-01', 'LEAD28MAR2009', 554, 'MCX'),
(19, '2009-04-01', '2009-04-01', 'NATURALGAS19MAR2009', 444, 'MCX'),
(20, '2009-04-01', '2009-04-01', 'NATURALGAS28MAR2009', 900, 'MCX'),
(21, '2009-04-01', '2009-04-01', 'NICKEL19MAR2009', 888, 'MCX'),
(22, '2009-04-01', '2009-04-01', 'NICKEL28MAR2009', 2200, 'MCX'),
(23, '2009-04-01', '2009-04-01', 'SILVER21MAR2009', 200, 'MCX'),
(24, '2009-04-01', '2009-04-01', 'SILVER28MAR2009', 300, 'MCX'),
(25, '2009-04-01', '2009-04-01', 'ZINC19MAR2009', 4500, 'MCX'),
(26, '2009-04-01', '2009-04-01', 'ZINC28MAR2009', 7800, 'MCX'),
(27, '2009-04-14', '2009-04-15', 'NIFTY28MAR2009', 1515, 'F_O'),
(28, '2009-04-14', '2009-04-15', 'REL28MAR2009', 1316, 'F_O'),
(29, '2009-04-14', '2009-04-15', 'COPPER19MAR2009', 1111, 'MCX'),
(30, '2009-04-21', '2009-04-22', 'GOLD19MAR2009', 16000, 'MCX');

-- --------------------------------------------------------

-- 
-- Table structure for table `storedbhav`
-- 

CREATE TABLE `storedbhav` (
  `stordId` int(11) NOT NULL,
  `storDate` varchar(20) character set utf8 NOT NULL,
  `status` varchar(10) character set utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `storedbhav`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `clientId2` int(6) default NULL,
  `firstName2` varchar(35) character set utf8 default NULL,
  `middleName2` varchar(35) character set utf8 default NULL,
  `lastName2` varchar(35) character set utf8 default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `price2` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default '0',
  `exchange` varchar(20) character set utf8 default NULL,
  `refTradeId` int(10) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxt`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtv1`
-- 

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtv1`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `clientId` varchar(50) NOT NULL default '',
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL default '',
  `deletepassword` varchar(50) NOT NULL default '',
  `userType` varchar(20) character set utf8 default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` (`clientId`, `name`, `password`, `deletepassword`, `userType`) VALUES 
('', 'shree', 'user1', '', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `vendor`
-- 

INSERT INTO `vendor` (`vendorId`, `vendor`, `firstName`, `middleName`, `lastName`, `address`, `phone`, `mobile`, `fax`, `email`, `deposit`, `currentBal`) VALUES 
(6, '_SELF', '_SELF', '', '', '', '', '', '', '', 0, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `itemId` varchar(50) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

-- 
-- Dumping data for table `vendorbrok`
-- 

INSERT INTO `vendorbrok` (`clientBrokId`, `vendor`, `itemId`, `oneSideBrok`, `brok1`, `brok2`) VALUES 
(96, '_SELF', 'SILVER', 0, NULL, NULL),
(97, '_SELF', 'GOLD', 0, NULL, NULL),
(98, '_SELF', 'CRUDEOIL', 0, NULL, NULL),
(99, '_SELF', 'COPPER', 0, NULL, NULL),
(100, '_SELF', 'GOLDM', 0, NULL, NULL),
(101, '_SELF', 'SILVERM', 0, NULL, NULL),
(102, '_SELF', 'NATURALGAS', 0, NULL, NULL),
(103, '_SELF', 'ZINC', 0, NULL, NULL),
(104, '_SELF', 'NICKEL', 0, NULL, NULL),
(105, '_SELF', 'MENTHAOIL', 0, NULL, NULL),
(106, '_SELF', 'POTATO', 0, NULL, NULL),
(107, '_SELF', 'NIFTY', 0, NULL, NULL),
(108, '_SELF', 'LEAD', 0, NULL, NULL),
(109, '_SELF', 'NIFTY', 10, 0, 0),
(110, '_SELF', 'NIFTY', 10, 0, 0),
(111, '_SELF', 'NIFTY', 10, 0, 0),
(112, '_SELF', 'NIFTY', 10, 0, 0),
(113, '_SELF', 'REL', 1, 0, 0),
(114, '_SELF', 'REL', 1, 0, 0),
(115, '_SELF', 'qwert', 12, 0, 0),
(116, '_SELF', 'qwert', 12, 0, 0),
(117, '_SELF', 'GGG', 300, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

