<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A></LI>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brokers</A></LI>
    <LI><A href="bankMasterList.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expense</A></LI>
    <LI><A href="exchangeAdd.php">Exchange</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A>
  <UL>
    <LI><A href="#">Mcx Trades</A>
    	<UL>
    	  <LI><A href="clientTradesMcx.php"                 >Client Trades</A></LI>
		    <LI><A href="brokerTradesMcx.php"                 >Broker Trades</A></LI>
    	  <LI><A href="clientTradesMcx.php?display=detailed">Client Detailed Trades</A></LI>
		    <LI><A href="clientTradesMcx.php?display=detailed">Broker Detailed Trades</A></LI>
    	  <LI><A href="clientTradesPer1sideMcx.php"         >Client Trades in % 1 Side</A></LI>
    	  <LI><A href="brokerTradesPer1sideMcx.php"         >Broker Trades in % 1 Side</A></LI>
    	  <LI><A href="clientTradesPer2side2BothMcx.php"    >Client Trades in % 2 Side</A></LI>
    	  <LI><A href="brokerTradesPer2side2BothMcx.php"    >Broker Trades in % 2 Side</A></LI>
    	  <LI><A href="clientTradesMcx2side.php"            >Client Trades in Rs 2 Side</A></LI>
    	</UL>
    </LI>
    <LI><A href="#">F_O Trades</A>
    	<UL>
        <LI><A href="clientTradesPer2side2fo.php">Client F_O Trades</A></LI>
		    <LI><A href="brokerTradesPer2side2fo.php">Broker F_O Trades</A></LI>
<!--        <LI><A href="clientTradesPer2side2BothF_O.php">Client F_O Trades 2side2Both</A></LI>
		    <LI><A href="brokerTradesPer2side2BothF_O.php">Broker F_O Trades 2side2Both</A></LI>
        <LI><A href="clientTradesPer2side2foHigh.php">Client F_O Trades High</A></LI>
        <LI><A href="clientTradesPer1sideF_O.php">Client F_O Trades 1 Side</A></LI>
		    <LI><A href="brokerTradesPer1sideF_O.php">Broker F_O Trades 1 Side</A></LI>
    	  <LI><A href="clientTradesF_O.php"        >Client F_O Trades In Rs</A></LI>-->
    	</UL>
    </LI>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="#">&nbsp;</A></LI>
    <?php
      if($tradeInNewWindow == 1)
      {
    ?>
        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=950, height=165, left=1, top=300'); return false;">Data Entry Mcx ClientId</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry MCX New</A></LI>
    <?php
      }
      else
      {
    ?>
        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX">Data Entry Mcx ClientId</A></LI>
        <LI><A href="addTrade.php?exchange=MCX">Data Entry MCX New</A></LI>
    <?php
      }
    ?>
    <?php
      if($tradeInNewWindow == 1)
      {
    ?>
        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O ClientId</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O New</A></LI>
    <?php
      }
      else
      {
    ?>
        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O">Data Entry F_O ClientId</A></LI>
        <LI><A href="addTrade.php?exchange=F_O">Data Entry F_O New</A></LI>
    <?php
      }
    ?>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
    <LI><A href="bulkChange.php">Bulk Change</A></LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="mnuStand.php"                                   >Standing</A></LI>
    <LI><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX In Rs</A></LI>
    <LI><A href="clientTradesPer1sideMcx.php?display=tradesPrint">Bill Print MCX In % </A></LI>
    <LI><A href="clientTradesPer1sideMcx.php?display=tradesPrintOnlyNet">Bill Print MCX In % Only Net</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=gross"      >Generate Bill F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrint">Bill Print F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrintOnlyNet">Bill Print F_O Only Net</A></LI>
<!--    <LI><A href="clientTradesF_O.php?display=tradesPrint"        >Bill Print F_O In Rs</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=tradesPrint">Bill Print F_O 1 side In %</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=tradesPrintOnlyNet">Bill Print F_O 1 side In % Only Net</A></LI>-->
    <LI><A href="#">Rollback</A></LI>
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <LI><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWiseGross"        >Script Gross MCX In Rs</A></LI>
    <LI><A href="clientTradesPer1sideMcx.php?display=itemWiseGross">Script Gross MCX In % </A></LI>
    <LI><A href="clientTradesMcx.php?display=gross"              >Bill Gross MCX In Rs</A></LI>
    <LI><A href="clientTradesPer1sideMcx.php?display=gross"      >Bill Gross MCX In %</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWisePL">Script wise PL MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending">Script Outstanding F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending2">Average Outstanding F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemWiseGross">Script Gross F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemWisePL">Script wise PL F_O</A></LI>
<!--    <LI><A href="clientTradesF_O.php?display=gross"              >Bill Gross F_O In Rs</A></LI>
    <LI><A href="clientTradesPer1sideF_O.php?display=gross"      >Bill Gross F_O 1 side In %</A></LI>-->
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="backup.php">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
    <LI><A href="#">ExtraMenu</A>
      <UL>
		    <LI><A href="ajxStoreLimitTradeMcx1.php">Data Entry Mcx Fast</A></LI>
		    <LI><A href="ajxStoreLimitTradeFO.php">Data Entry FO Fast</A></LI>
        <LI><A href="storeLimitTradeMcx.php">Data Entry Mcx Slow</A></LI>
        <LI><A href="storeLimitTradeF_O.php">Data Entry F_O Slow</A></LI>
      </UL>
    </LI>
  </UL>
</LI>

<LI><A href="selectDtSession.php">Date Range</A>
</LI>
<LI><A href="logout.php">Logout</A>
</LI>

</UL>
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>