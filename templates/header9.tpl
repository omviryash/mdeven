<TABLE cellPadding="8" cellspacing="0" border="1">
<TR>
  <TD>Masters</TD>
  <TD><A href="brokerage.php">Items</A></TD>
  <TD><A href="expiryList.php">Expiry Dates</A></TD>
  <TD><A href="clientList.php">Clients and Brokers</A></TD>
  <TD><A href="bankMasterList.php">Bank Masters</A></TD>
  <TD><A href="otherExpAdd.php">Other Expense</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Entry : </TD>
  <TD><A href='' onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=160, left=1, top=300'); return false;">Data Entry Mcx</A></TD>
  <TD><A href='' onClick="tradeWindow=window.open('ajxStoreLimitTradeF_O1.php?useTpl=txtbx', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=160, left=1, top=300'); return false;">Data Entry F_O</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Trades</TD>
  <TD><A href="clientTradesMcx.php"                 >Client Mcx Trades</A></TD>
  <TD><A href="brokerTradesMcx.php"                 >Broker Mcx Trades</A></TD>
  <TD><A href="clientTradesMcx.php?display=detailed">Client Detailed Trades</A></TD>
  <TD><A href="clientTradesMcx.php?display=detailed">Broker Detailed Trades</A></TD>
  <TD><A href="clientTradesPer2side2fo.php">Client F_O Trades</A></TD>
  <TD><A href="brokerTradesPer2side2fo.php">Broker F_O Trades</A></TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Other Options</TD>
  <TD><A href="tradeListForCrossChecking.php">Confirm</A></TD>
  <TD><A href="orderList.php">Order List</A></TD>
  <TD><A href="attachNameToTxtData.php">Pending Trades</A></TD>
  <TD><A href="attachNameToTxtDataAll.php">Change Name</A></TD>
  <TD><A href="deleteAll.php">Delete Trades</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Settlement</TD>
  <TD><A href="mnuStand.php"                                   >Standing</A></TD>
  <TD><A href="clientTradesMcx.php?display=gross"              >Generate Bill MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=gross"      >Generate Bill F_O</A></TD>
  <TD><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=tradesPrint">Bill Print F_O</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Reports</TD>
  <TD><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=itemPending">Script Outstanding F_O</A></TD>
  <TD><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=itemPending2">Average Outstanding F_O</A></TD>
  <TD><A href="clientTradesMcx.php?display=itemWiseGross">Script Gross MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=itemWiseGross">Script Gross F_O</A></TD>
  <TD><A href="clientTradesMcx.php?display=gross">Bill Gross MCX</A></TD>
  <TD><A href="clientTradesPer2side2fo.php?display=gross">Bill Gross F_O</A></TD>
</TR>
<TR>
  <TD>Accounts</TD>
  <TD><A href="accSummary.php">Account Last Balance</A></TD>
  <TD><A href="mnuAccount.php">Money Transaction</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>Utilities</TD>
  <TD><A href="writeTradesFile.php">Online Backup</A></TD>
  <TD><A href="backup.php">Backup</A></TD>
  <TD><A href="links.php">Useful Links</A></TD>
  <TD><A href="calc.php">Calculator</A></TD>
  <TD><A href="changePassword.php">Change Password</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD><A href="selectDtSession.php">Date Range</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD><A href="logout.php">Logout</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
</TABLE>