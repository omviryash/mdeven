<?php
  include "./etc/om_config.inc";
//  include "./etc/config.inc";
  $smarty = new SmartyWWW();
  $clientId = "";
  $name = "";
  if(isset($_POST['btnSubmit']))
  {
    $fromDate = $_POST['fromDateYear']."-".$_POST['fromDateMonth']."-".$_POST['fromDateDay'];
    $toDate   = $_POST['toDateYear']."-".$_POST['toDateMonth']."-".$_POST['toDateDay'];
    $deleteQuery = "DELETE FROM tradetxt WHERE 1=1";
    $deleteQuery .= " AND tradeDate >= '".$fromDate."'";
    $deleteQuery .= " AND tradeDate <= '".$toDate."'";
    if($_POST['clientId'] > 0)
      $deleteQuery .= " AND clientId = '".$_POST['clientId']."'";
    if($_POST['exchange'] != "All")
    {
      $deleteQuery .= " AND exchange = '".$_POST['exchange']."'";
      if($_POST['itemId'] != "All")
        $deleteQuery .= " AND itemId = '".$_POST['itemId']."'";
    }

	  if(isset($_POST['trades']) && isset($_POST['open']) && isset($_POST['close']))
	    $deleteQuery .= " AND ( standing = 0 OR standing = -1 OR standing = 1)";
	  elseif(isset($_POST['trades']) && isset($_POST['open']))
	    $deleteQuery .= " AND ( standing = 0 OR standing = -1)";
	  elseif(isset($_POST['trades']) && isset($_POST['close']))
	    $deleteQuery .= " AND ( standing = 0 OR standing = 1)";
	  elseif(isset($_POST['open']) && isset($_POST['close']))
	    $deleteQuery .= " AND ( standing = -1 OR standing = 1)";
	  elseif(isset($_POST['trades']))
	    $deleteQuery .= " AND standing = 0";
	  elseif(isset($_POST['open']))
	    $deleteQuery .= " AND standing = -1";
	  elseif(isset($_POST['close']))
	    $deleteQuery .= " AND standing = 1";
    $deleteResult = mysql_query($deleteQuery);
    if(!$deleteResult)
      echo mysql_error().$deleteQuery;
  }
  
  $selectClient = "SELECT * FROM client ORDER BY firstName";
  $resultClient = mysql_query($selectClient);
  $i=0;
  while($rowClient = mysql_fetch_array($resultClient))
  {
  	$clientId[$i] = $rowClient['clientId'];
  	$name[$i] = $rowClient['firstName']." ".$rowClient['middleName']." ".$rowClient['lastName'];
  	$i++;
  }

  $selectExchangeQuery = "SELECT * FROM exchange
                          ORDER BY exchange";
  $selectExchangeQueryResult = mysql_query($selectExchangeQuery);
  $i = 0;$j = 0;
  $exchangeId[$i] = 0;
  $exchange[$i]   = "All";
  $i++;
  $itemId[$i][$j]   = "All";
  $itemName[$i][$j] = "All";

  while($row = mysql_fetch_array($selectExchangeQueryResult))
  {
  	$exchangeId[$i] = $row['exchangeId'];
  	$exchange[$i]   = $row['exchange'];
  	$selectItem = "SELECT * FROM item 
  	               WHERE exchangeId = '".$row['exchangeId']."'
  	               ORDER BY item";
		$resultItem = mysql_query($selectItem);
		$j = 0;
		$itemId[0][0]   = "All";
    $itemName[0][0] = "All";
		$itemId[$i][$j]   = "All";
    $itemName[$i][$j] = "All";
    $j++;
		while($rowItem = mysql_fetch_array($resultItem))
		{
			$itemId[$i][$j]   = $rowItem['itemId'];
			$itemName[$i][$j] = $rowItem['item'];
			$j++;
		}
  	$i++;
  }
  $smarty->assign("clientId",$clientId);
  $smarty->assign("name",$name);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("exchangeId",$exchangeId);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);

  $smarty->display("deleteAll.tpl");
?>