<HTML>
<HEAD><TITLE>Om !!!</TITLE>
  <STYLE src="./templates/styles.css"></STYLE>
</HEAD>
<BODY bgcolor="#B0D8FF">
<FORM name="form1" action="{$PHP_SELF}" method=POST>
<A href="./accTransList.php">List</A>&nbsp;&nbsp;<A href="./mnuAccount.php">Menu</A><BR><BR>
<TABLE>
    <TR>
      <TD>Name</TD>
      <TD><SELECT name = "cboName">
            {section name=sec1 loop=$fName}
              <OPTION value={$clId[sec1]}>
                {$fName[sec1]}
              </OPTION>
            {/section}
        </SELECT></TD>
    </TR>
    <TR>
      <TD>Date</TD>
      <TD> {html_select_date prefix="entryDate" start_year=1990 end_year=2025 day_format="%02d" month_format="%b" field_order="DMY"}</TD>
    </TR>
   <TR>
   	<TD>Transaction In</TD>
   	<TD><select name="transMode">
   				<option value="Cash">- Cash -</option>
   				{section name=secBank loop=$bank}
   				    <OPTION value="{$bank[secBank]}">
                {$bank[secBank]}
              </OPTION>
   				{/section}
   		</select></Td>
   </TR>
   <TR>
     <TD><INPUT type="radio" name="r1" value="d" checked>Credit
         &nbsp;&nbsp;
         <INPUT type="radio" name="r1" value="w">Debit</TD>
     <TD><INPUT type="text" name="txtdwAmount"></TD>
   </TR>
   <TR>
     <TD>Transaction Type : </TD>
     <TD>
         <INPUT type="radio" name="transType" value="Other" checked>Other
         &nbsp;&nbsp;&nbsp;
         <INPUT type="radio" name="transType" value="Margin">Margin
     </TD>
   </TR>
   <TR>
   	<TD>Exchange</TD>
   	<TD><select name="exchange">
   				{section name=secExchange loop=$exchangeArray}
   				    <OPTION value="{$exchangeArray[secExchange]}">
                {$exchangeArray[secExchange]}
              </OPTION>
   				{/section}
   		</select></Td>
   </TR>
   <TR>
     <TD>Note : </TD>
     <TD><INPUT type="text" name="itemIdExpiryDate" size="50"></TD>
   </TR>
   <TR>
      <TD><INPUT type=submit value="Submit Me">
         <INPUT type=reset value="Reset"></TD>
   </TR>
</TABLE>
</FORM>
</BODY>
</HTML>