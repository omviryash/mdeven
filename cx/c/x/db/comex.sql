-- phpMyAdmin SQL Dump
-- version 2.10.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Apr 10, 2010 at 11:13 AM
-- Server version: 5.0.67
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `comex`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) NOT NULL default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) NOT NULL default '',
  `dwAmount` float NOT NULL default '0',
  `plStatus` char(2) NOT NULL default '',
  `plAmount` float NOT NULL default '0',
  `transactionDate` date default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `passwd` varchar(20) character set utf8 default NULL,
  `oneSideBrok` double default NULL,
  `remiser` int(6) default NULL,
  `remiserBrok` varchar(40) character set utf8 default NULL,
  `remiserBrokIn` varchar(20) character set utf8 default NULL,
  `clientBroker` int(6) default NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `client`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(10) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- 
-- Dumping data for table `expiry`
-- 

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES 
(24, 'CxGold', '01DEC2010', NULL),
(25, 'CxSilver', '01DEC2010', NULL),
(26, 'CxCrude', '01DEC2010', NULL),
(27, 'CxEuro', '01DEC2010', NULL),
(29, 'GBP', '01DEC2010', NULL),
(30, 'CxNatu', '01DEC2010', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `general`
-- 

INSERT INTO `general` (`generalId`, `filePath`, `fileName`) VALUES 
(1, 'C:\\MCXTrade\\Client\\OnlineBackup\\Trades', '07102005TRD.TXT');

-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` int(6) default NULL,
  `brok2` int(6) default NULL,
  `oneSideBrok` int(6) default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchangeId` int(6) default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `multiply` float default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchangeId`, `exchange`, `multiply`) VALUES 
('CxGold', 'CxGold', 'CxGold', 40, 40, 40, 1, 1, 100, 400, 520, NULL, NULL, NULL, NULL),
('CxSilver', 'CxSilver', 'CxSilver', 40, 40, 40, 1, 1, 50, 670, 820, NULL, NULL, NULL, NULL),
('CxCrude', 'CxCrude', 'CxCrude', 40, 40, 40, 1, 1, 500, 45, 85, NULL, NULL, NULL, NULL),
('CxEuro', 'CxEuro', 'CxEuro', 40, 40, 40, 1, 1, 125000, 0.5, 3, NULL, NULL, NULL, NULL),
('CxNatu', 'CxNatu', 'CxNatu', 0, 0, 35, 1, 1, 10, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `settingsKey` varchar(30) character set utf8 default NULL,
  `settingsValue` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `settings`
-- 

INSERT INTO `settings` (`settingsId`, `settingsKey`, `settingsValue`) VALUES 
(1, 'uploadFileWorks', '1'),
(2, 'expiryDisplay', 'monthOnly'),
(3, 'profitBankRate', '44.5'),
(4, 'lossBankRate', '45'),
(5, 'clientFieldInTxt', 'ownClient'),
(6, 'clientFieldInTxt', 'userRemarks'),
(7, 'takeTimeEntry', '0'),
(8, 'takeTradeNote', '0'),
(9, 'qtyInLots', '0'),
(10, 'useItemPriceRange', '1'),
(11, 'vendorAsClient', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `standing`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `clientId2` int(6) default NULL,
  `firstName2` varchar(35) character set utf8 default NULL,
  `middleName2` varchar(35) character set utf8 default NULL,
  `lastName2` varchar(35) character set utf8 default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `price2` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) character set utf8 default NULL,
  `vendor` varchar(50) character set utf8 default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `refTradeId` int(6) default NULL,
  `selfRefId` int(6) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxt`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `vendor`
-- 

INSERT INTO `vendor` (`vendorId`, `vendor`, `firstName`, `middleName`, `lastName`, `address`, `phone`, `mobile`, `fax`, `email`, `deposit`, `currentBal`) VALUES 
(2, '_SELF  ', '_SELF', '', '', '', '', '', '', '', 0, NULL),
(3, 'zST  ', 'zST', '', '', '', '', '', '', '', 0, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) character set utf8 default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

-- 
-- Dumping data for table `vendorbrok`
-- 

INSERT INTO `vendorbrok` (`clientBrokId`, `vendor`, `itemId`, `oneSideBrok`, `brok1`, `brok2`) VALUES 
(56, 'ZStand', 'CxGold', 0, NULL, NULL),
(57, 'ZStand', 'CxSilver', 0, NULL, NULL),
(58, 'ZStand', 'CxCrude', 0, NULL, NULL),
(59, 'ZStand', 'CxEuro', 0, NULL, NULL),
(60, 'ZStand', 'GBP', 0, NULL, NULL),
(61, '_SELF  ', 'CxGold', 0, NULL, NULL),
(62, '_SELF  ', 'CxSilver', 0, NULL, NULL),
(63, '_SELF  ', 'CxCrude', 0, NULL, NULL),
(64, '_SELF  ', 'CxEuro', 0, NULL, NULL),
(65, 'zST  ', 'CxGold', 15, NULL, NULL),
(66, 'zST  ', 'CxSilver', 15, NULL, NULL),
(67, 'zST  ', 'CxCrude', 15, NULL, NULL),
(68, 'zST  ', 'CxEuro', 15, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

ALTER TABLE  `vendortrades` ADD  `confirmed` TINYINT( 4 ) NULL ,
ADD  `exchange` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
ADD  `refTradeId` INT( 6 ) NULL ,
ADD  `selfRefId` INT( 6 ) NULL ;
ALTER TABLE  `vendortemp` ADD  `confirmed` TINYINT( 4 ) NULL ,
ADD  `exchange` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
ADD  `refTradeId` INT( 6 ) NULL ,
ADD  `selfRefId` INT( 6 ) NULL ;

ALTER TABLE  `vendortemp` ADD  `clientId2` INT( 6 ) NULL AFTER  `lastName` ,
ADD  `firstName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `clientId2` ,
ADD  `middleName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `firstName2` ,
ADD  `lastName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `middleName2` ;

ALTER TABLE  `vendortrades` ADD  `clientId2` INT( 6 ) NULL AFTER  `lastName` ,
ADD  `firstName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `clientId2` ,
ADD  `middleName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `firstName2` ,
ADD  `lastName2` VARCHAR( 35 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `middleName2` ;

ALTER TABLE  `vendortemp` ADD  `price2` FLOAT NULL AFTER  `price` ;
ALTER TABLE  `vendortrades` ADD  `price2` FLOAT NULL AFTER  `price` ;

update item   set exchange='CX';
update expiry set exchange='CX';