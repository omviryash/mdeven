<?php
include "etc/om_config.inc";
session_start();
$goTo = "bulkChange";

if(!isset($_SESSION['user']))
  header("Location: login.php");
else
{
  $smarty= new SmartyWWW();
  if(!isset($_SESSION['fromDate']))
    header("location:selectDtSession.php?goTo=bulkChange");
  
  if(isset($_POST['tradeId']) && !isset($_POST['clientForSort']))
  {
    $elementNo   = 0;
    $tradeIds    = "";
    $setNewValue = "";
    
  	foreach($_POST['tradeId'] as $key=>$value)
  	{
  	  if($elementNo == 0)
  	    $tradeIds .= $value;
  	  else
  	    $tradeIds .= ", ".$value;
  	  
  	  $elementNo++;
  	}
  	
  	if($_POST['clientIdForTradeId'] != 0)
  	{
    	$setNewValue .= "clientId = ".$_POST['clientIdForTradeId'].",";
    	
      $clientNamesQuery = "SELECT * FROM client WHERE clientId=". $_POST['clientIdForTradeId'];
      $clientNamesResult = mysql_query($clientNamesQuery);
      while($clientNamesRow = mysql_fetch_array($clientNamesResult))
      { 
        $setNewValue .= " firstName = '".$clientNamesRow['firstName']."', middleName = '".$clientNamesRow['middleName']."', lastName ='".$clientNamesRow['lastName']."',";
      }
    }
    
    if($_POST['changeDateMonth'] != '' && $_POST['changeDateDay']!= '' && $_POST['changeDateYear'])
    {
      $setNewValue .= " tradeDate = '".$_POST['changeDateYear']."-".$_POST['changeDateMonth']."-".$_POST['changeDateDay']."',";
    }
    
    if($_POST['buySell'] != '0')
      $setNewValue .= " buySell = '".$_POST['buySell']."',";
    
    if($_POST['item'] != '0')
      $setNewValue .= " itemId = '".$_POST['item']."',";
      
    if(isset($_POST['expiry']) && $_POST['expiry'] != 0)
      $setNewValue .= " expiryDate = '".$_POST['expiry']."',";
    
    if(is_numeric($_POST['price']) && $_POST['price'] > 0)
      $setNewValue .= " price = ".$_POST['price'].",";
    
    if(strlen($setNewValue) > 0)
    {
      $updateQuery  = "UPDATE tradetxt SET ".substr($setNewValue,0,-1)."
                              WHERE tradeId IN (".$tradeIds.")";
      $updateResult = mysql_query($updateQuery);
    }
  }
  
  $clientNamesQuery = "SELECT * FROM client
                        ORDER BY firstName, middleName, lastName";
  $clientNamesResult = mysql_query($clientNamesQuery);
  
  $clientName = array();
  $a = 0;
  while($clientRow = mysql_fetch_array($clientNamesResult))
  {
    $clientName['id'][$a]     = $clientRow['clientId'];
    $clientName['name'][$a] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
    $a++;
  }
  
  $selectExchange = "SELECT DISTINCT(exchange) FROM expiry
                      ORDER By exchange";
  $selectExchangeRes = mysql_query($selectExchange); 
  $exchange = array();
  $b = 0;
  while($exchangeRow = mysql_fetch_array($selectExchangeRes))
  {
    $exchange['exchange'][$b] = $exchangeRow['exchange'];
    $b++;
  }
  //This for sort by client name : Start
  $addAndCondition = "";
  $selectClient = 0;
  if(isset($_POST['clientForSort']) && $_POST['clientForSort'] != 0)
  {
    $selectClient = $_POST['clientForSort'];
    $addAndCondition = " AND clientId = ".$_POST['clientForSort'];
  }
  
  //This for sort by client name : End  
  $clientIdQuery = "SELECT * FROM tradetxt
                     WHERE clientId >= 0 ".$addAndCondition;
  $whereGiven = true;
  
   if(isset($_SESSION['fromDate']))
   {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
     if($whereGiven)
       $clientIdQuery .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
     else
     {
       $clientIdQuery .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
       $whereGiven = true;
     }
   }

  $clientIdQuery .= " ORDER BY tradeDate DESC, tradeTime, tradeId;";

  $clientIdResult = mysql_query($clientIdQuery);
  $client = array();
  $a = 0;
  while($clientIdRow = mysql_fetch_array($clientIdResult))
  {
    $client[$a]['id']         = $clientIdRow['tradeId'];
    $client[$a]['name']       = $clientIdRow['firstName']." ".$clientIdRow['middleName']." ".$clientIdRow['lastName'];
    $client[$a]['dateTime']   = $clientIdRow['tradeDate'].' '.$clientIdRow['tradeTime'];
    $client[$a]['buySell']    = $clientIdRow['buySell'];
    $client[$a]['itemId']     = $clientIdRow['itemId'];
    $client[$a]['qty']        = $clientIdRow['qty'];
    $client[$a]['price']      = $clientIdRow['price'];
    $client[$a]['expiryDate'] = $clientIdRow['expiryDate'];
    $client[$a]['tradeRefNo'] = $clientIdRow['tradeRefNo'];
    $a++;
  }
  
  $smarty->assign("goTo",$goTo);
  $smarty->assign("clientName",$clientName);
  $smarty->assign("client",$client);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("selectClient",$selectClient);
  $smarty->display("bulkChange.tpl");
}
?>