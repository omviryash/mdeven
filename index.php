<?php
session_start();
if(!isset($_SESSION['user']))
  header("Location: login.php");
else
{
  include "./templates/headerMain.tpl";
  include "./etc/om_config.inc";
  include "./header.php";
  include "./templates/footer1.tpl";
}
?>