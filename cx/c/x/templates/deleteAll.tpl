{include file="headerMain.tpl"}
<script type="text/javascript">
  {literal}
  function itemChange(theObject)
  {
    var form = theObject.form;
    for(i=0;i<form.elements.length;i++)
    {
      if(form.elements[i]==theObject)
      {
        exchange = document.form1.exchange;
        itemId = document.form1.itemId;
      }
    }
    itemId.options.length = 0;
    //itemId.options[{%item.index%}]=new Option("All","0");
    {/literal}
    {section name="exchange" loop=$exchange}
    if(exchange.selectedIndex == {%exchange.index%} )
    {literal}
    {
      {/literal}
      {section name="item" loop=$itemId[exchange]}
        itemId.options[{%item.index%}]=new Option("{$itemName[exchange][item]}","{$itemId[exchange][item]}");
      {/section}
      {literal}
    }
    {/literal}
    {/section}
    {literal}
  }
{/literal}
</script>
<div align="right"><a href="index.php">Home</a></div>
	<FORM name="form1" action="{$PHP_SELF}" method="POST">
	<TABLE>
    <TR>
      <TD align="right">Client : </TD>
      <TD>
      	<select name="clientId">
      		<option value="0">ALL</option>
      		{section name="sec1" loop=$clientId}
      			<option value="{$clientId[sec1]}">{$name[sec1]}</option>
      		{/section}
      	</select>
      </TD>
    </TR>
    <TR>
      <TD align="right">From Date : </TD>
      <TD>{html_select_date prefix="fromDate" start_year="+2" end_year="-2" field_order="dmy"}</TD>
      <TD align="right">&nbsp;&nbsp;&nbsp;To Date : </TD>
      <TD>{html_select_date prefix="toDate" start_year="+2" end_year="-2" field_order="dmy"}</TD>
    </TR>
    <TR>
      <TD align="right">Exchange : </TD>
      <TD>
      	<select name="exchange" onChange="itemChange(this);">
      		{html_options values="$exchange" output="$exchange"}
      	</select>
      </TD>
      <TD align="right">Item : </TD>
      <TD>
      	<select name="itemId">
      		<OPTION value="All">All</OPTION>
      	</select>
      </TD>
    </TR>
    <TR>
    	<TD align="right">Standing : </TD>
    	<TD>
    	  <INPUT type="checkbox" name="trades" value="0" CHECKED>Trades&nbsp;&nbsp;&nbsp;&nbsp;
    	  <INPUT type="checkbox" name="open" value="-1" CHECKED>Open&nbsp;&nbsp;&nbsp;&nbsp;
    	  <INPUT type="checkbox" name="close" value="1" CHECKED>Close
    	</TD>
    </TR>
    <TR>
    	<TD></TD>
    	<TD><INPUT type="submit" name="btnSubmit" value="Go !!"></TD>
    </TR>
    	
  </TABLE>
</FORM>
<script type="text/javascript">
	document.form1.clientId.focus();
</script>
{include file="footer1.tpl"}
