<?php
  include "./etc/om_config.inc";

  $smarty= new SmartyWWW();
  
  if(isset($_GET['listOnly']))
    $_SESSION['listOnly'] = $_GET['listOnly'];
  else
    $_SESSION['listOnly'] = 0;
  $_SESSION['orderType']="";
  
  if(!isset($_SESSION['orderStatus']))
    $_SESSION['orderStatus']='Pending';
  
  $_SESSION['orderValidity']="";
  $_SESSION['clientName']="";
  $_SESSION['item']="";
  $_SESSION['expiryDate']="";
    
  if(isset($_POST['btnSubmit']))
  {
    $_SESSION['toDate']=$_POST['toDateYear']."-".$_POST['toDateMonth']."-".$_POST['toDateDay'];
    $_SESSION['fromDate']=$_POST['fromDateYear']."-".$_POST['fromDateMonth']."-".$_POST['fromDateDay'];
    $_SESSION['clientName']=$_POST['cboClientName'];
    $_SESSION['item']=$_POST['cboItem'];
    
    if(isset($_POST['cboExpiryDate']))
    {
      $_SESSION['expiryDate']=$_POST['cboExpiryDate'];
      //echo $_SESSION['expiryDate'];
    }
    else
      $_SESSION['expiryDate']="";
    
    $_SESSION['orderType']=$_POST['cboOrderType'];
    $_SESSION['orderValidity']=$_POST['cboOrderValidity'];
    $_SESSION['orderStatus']=$_POST['cboStatus'];
  }

  if(!isset($_SESSION['toDate']))
  {
    $_SESSION['toDate']=date("Y-m-d");
    $_SESSION['fromDate']=date("Y-m-d");
  }
  //SELECTION OF CLIENT:START
  $selectClient="SELECT * FROM client
                  ORDER BY firstName,middleName,lastName
                ";
  
  $resultClient=mysql_query($selectClient);
  if(!$resultClient)
  {
    echo "No Client Selected";
  }
  else
  {
    $j=0;
    while($row=mysql_fetch_array($resultClient))
    {
      $clientId[$j]=$row['clientId'];
      $clientName[$j]=$row['firstName']." ".$row['middleName']." ".$row['lastName'];
      $j++;
      $smarty->assign("clientName",$clientName);
      $smarty->assign("clientId",$clientId);
    }
  }
  //SELECTION OF CLIENT:END
  
  //SELECTION OF ITEM:START
  
  $selectItem="SELECT * FROM item
                ORDER BY itemId
              ";
  
  $resultItem=mysql_query($selectItem);
  if(!$resultItem)
  {
    echo "No Item Selected";
  }
  else
  {
    $k=0;
    while($row=mysql_fetch_array($resultItem))
    {
      $itemId[$k]=$row['itemId'];
      
      //SELECTION OF EXPIRYDATE:START
      
      $selectExpiry="SELECT * FROM expiry WHERE itemId='".$itemId[$k]."'";
      
      $resultExpiry=mysql_query($selectExpiry);
      if(!$resultExpiry)
      {
        echo "No Expiry Selected";
      }
      else
      {
        $l=0;
        while($row=mysql_fetch_array($resultExpiry))
        {
          $expiryDate[$k][$l]=$row['expiryDate'];
          $expiryDateSelected[$l]=$row['expiryDate'];
          $l++;
          $smarty->assign("expiryDate",$expiryDate);
          //$smarty->assign("expiryDateSelected",$expiryDateSelected);
        }
      }
      //SELECTION OF EXPIRYDATE:END
  
      $k++;
      $smarty->assign("itemId",$itemId);
      $smarty->assign("j",$j);
      $smarty->assign("k",$k);
    }
  }
  //SELECTION OF ITEM:END
  
  $selectQuery="SELECT * FROM orders WHERE '1'='1'";
  if(isset($_POST['btnSubmit']))
  {
    if($_POST['cboClientName']!="All")
      $selectQuery = $selectQuery." AND clientId='".$_POST['cboClientName']."'";
    if($_POST['cboItem']!="All")
      $selectQuery = $selectQuery." AND itemId='".$_POST['cboItem']."'";
    if(isset($_POST['cboExpiryDate']) && $_POST['cboExpiryDate']!="")
      $selectQuery = $selectQuery." AND expiryDate='".$_POST['cboExpiryDate']."'";
    if($_POST['cboOrderType']!="All")
      $selectQuery = $selectQuery." AND orderType='".$_POST['cboOrderType']."'";
    if($_POST['cboOrderValidity']!="All")
      $selectQuery = $selectQuery." AND orderValidity='".$_POST['cboOrderValidity']."'";
  }
  if($_SESSION['orderStatus']!="All")
    $selectQuery = $selectQuery." AND orderStatus='".$_SESSION['orderStatus']."'";
     
  $selectQuery=$selectQuery." AND ((orderDate BETWEEN '".$_SESSION['toDate']."'
                AND '".$_SESSION['fromDate']."') 
                
                OR ((orderValidTillDate BETWEEN '".$_SESSION['toDate']."' 
                   AND '".$_SESSION['fromDate']."') 
                OR ('".$_SESSION['toDate']."' BETWEEN orderDate AND orderValidTillDate)
                OR ('".$_SESSION['fromDate']."' BETWEEN orderDate AND orderValidTillDate)
                OR (orderDate <='".$_SESSION['toDate']."' AND ordervalidity='GTC')))";
  $selectNew=$selectQuery;
  
  $selectQuery .= " ORDER BY orderId";
  $result=mysql_query($selectQuery);
  
  $orderValidTillDate = array();
  
  $i=0;
  if(!$result || mysql_num_rows($result)=="0")
  {
    $message="No Records Found";
    $smarty->assign("message",$message);
  }
  if($result && mysql_num_rows($result)!=0)
  {
    while($row=mysql_fetch_array($result))
    {
      $orderId[$i]=$row['orderId'];
      $clientIdSelected[$i]=$row['clientId'];
      $clientNameSelected[$i]=$row['firstName']." ".$row['middleName']." ".$row['lastName'];
      $itemSelected[$i]=$row['itemId'];
      $qty[$i]=$row['qty'];
      $price[$i]=$row['price'];
      $triggerPrice[$i]=$row['triggerPrice'];
      $buySell[$i]=$row['buySell'];
      $expiryDateSelected[$i]=$row['expiryDate'];
      $orderStatus[$i]=$row['orderStatus'];
      $orderDate[$i]=substr($row['orderDate'],8,2)."-".substr($row['orderDate'],5,2)."-".substr($row['orderDate'],0,4);
      $orderTime[$i]=$row['orderTime'];
      $orderType[$i]=$row['orderType'];
      $orderValidity[$i]=$row['orderValidity'];
      if($row['orderValidity'] == 'GTD')
        $orderValidTillDate[$i] = substr($row['orderValidTillDate'],8,2)."-".substr($row['orderValidTillDate'],5,2)."-".substr($row['orderValidTillDate'],0,4);
      else
        $orderValidTillDate[$i] = '-';
      $userRemarks[$i]  = $row['userRemarks'];
      $vendor[$i]  = $row['vendor'];
      $i++;
    }
    
    $smarty->assign("orderId",$orderId);
    $smarty->assign("clientIdSelected",$clientIdSelected);
    $smarty->assign("clientNameSelected",$clientNameSelected);
    $smarty->assign("itemSelected",$itemSelected);
    $smarty->assign("qty",$qty);
    $smarty->assign("price",$price);
    $smarty->assign("triggerPrice",$triggerPrice);
    $smarty->assign("buySell",$buySell);
    $smarty->assign("expiryDateSelected",$expiryDateSelected);
    $smarty->assign("orderStatus",$orderStatus);
    $smarty->assign("orderDate",$orderDate);
    $smarty->assign("orderTime",$orderTime);
    $smarty->assign("orderType",$orderType);
    $smarty->assign("orderValidity",$orderValidity);
    $smarty->assign("orderValidTillDate",$orderValidTillDate);
    $smarty->assign("userRemarks",$userRemarks);
    $smarty->assign("vendor",$vendor);
    
    $smarty->assign("i",$i);
  }
  
  $smarty->assign("listOnly",$_SESSION['listOnly']);
  $smarty->assign("dateTo",$_SESSION['toDate']);
  $smarty->assign("dateFrom",$_SESSION['fromDate']);
  $smarty->assign("clientNameBack",$_SESSION['clientName']);
  $smarty->assign("itemBack",$_SESSION['item']);
  $expiryDateBack=$_SESSION['expiryDate'];
  $smarty->assign("expiryDateBack",$expiryDateBack);
  $smarty->assign("orderTypeBack",$_SESSION['orderType']);
  $smarty->assign("orderValidityBack",$_SESSION['orderValidity']);
  $smarty->assign("orderStatusBack",$_SESSION['orderStatus']);
    
    $selectNew .=" ORDER BY itemId,expiryDate";
    $result1=mysql_query($selectNew);
    $num=0;
    $num1=0;
    $num1--;
    $oldItemId="";
    $oldExpiryDate="";
    $buyQty[0]=0;
    $sellQty[0]=0;
    
    while($orderRow=mysql_fetch_array($result1))
    {
      $buySell[$num]=$orderRow['buySell'];
      //echo $buySell[$num];
      $itemId[$num]=$orderRow['itemId'];
      //echo $itemId[$num];
      $qty[$num]=$orderRow['qty'];
      $expiryDate[$num]=$orderRow['expiryDate'];
      $price[$num]=$orderRow['price'];
      $triggerPrice[$num]=$orderRow['triggerPrice'];
      if($oldItemId!=$orderRow['itemId'] || $oldExpiryDate!=$orderRow['expiryDate'])
      {
        $num1++;
        $oldItemId = "";
        $oldItemId = $orderRow['itemId'];
        $oldExpiryDate="";
        $oldExpiryDate=$orderRow['expiryDate'];
        $rowItem[$num1]=$orderRow['itemId'];
        $rowExpiryDate[$num1]=$orderRow['expiryDate'];
        $buyPrice[$num1]=0;
        $sellPrice[$num1]=0;
        $buyQty[$num1]=0;
        $sellQty[$num1]=0;
        $pending[$num1]=0;
        $canceled[$num1]=0;
        $executed[$num1]=0;
        $rl[$num1]=0;
        $sl[$num1]=0;
        $eos[$num1]=0;
        $gtd[$num1]=0;
        $gtc[$num1]=0;
        
      }
      if($buySell[$num]=='Buy')
      {
        $buyPrice[$num1] =  $buyPrice[$num1] + $price[$num];
        $buyQty[$num1] = $buyQty[$num1] + $qty[$num];
        
      }
      else
      { 
        $sellPrice[$num1] =  $sellPrice[$num1] + $price[$num];
        $sellQty[$num1] = $sellQty[$num1] + $qty[$num];
        //echo $sellQty[$num1];
      }
      
      if($orderRow['orderStatus']=='Pending')
        $pending[$num1]++;
      else if($orderRow['orderStatus']=='Cancel')
        $canceled[$num1]++;
      else
        $executed[$num1]++;
      
      if($orderRow['orderType']=='RL')
        $rl[$num1]++;
      else
        $sl[$num1]++;
      
      if($orderRow['orderValidity']=='EOS')
        $eos[$num1]++;
      else if($orderRow['orderValidity']=='GTD')
        $gtd[$num1]++;
      else
        $gtc[$num1]++;
      
      $num++;
  
  $smarty->assign("rowItem",$rowItem);
  //echo $rowItem[0];
  $smarty->assign("rowExpiryDate",$rowExpiryDate);
  $smarty->assign("pending",$pending);
  $smarty->assign("canceled",$canceled);
  $smarty->assign("executed",$executed);
  $smarty->assign("rl",$rl);
  $smarty->assign("sl",$sl);
  $smarty->assign("eos",$eos);
  $smarty->assign("gtd",$gtd);
  $smarty->assign("gtd",$gtd);
  $smarty->assign("gtc",$gtc);
  $smarty->assign("num",$num);
  $smarty->assign("num1",$num1);
  //echo $num1;
}
  
  $smarty->assign("buyQty",$buyQty);
  $smarty->assign("sellQty",$sellQty);
  
  $smarty->display("orderList.tpl");
?>