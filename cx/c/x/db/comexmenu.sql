-- phpMyAdmin SQL Dump
-- version 2.10.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 04, 2010 at 10:44 AM
-- Server version: 5.0.67
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `comexmenu`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bankmaster`
-- 

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL auto_increment,
  `bankName` varchar(60) character set utf8 NOT NULL default '',
  `phone1` varchar(12) character set utf8 NOT NULL default '',
  `phone2` varchar(12) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`bankId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bankmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `bhavcopy`
-- 

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL auto_increment,
  `exchange` varchar(30) NOT NULL default '',
  `bhavcopyDate` date NOT NULL default '0000-00-00',
  `sessionId` varchar(15) NOT NULL default '',
  `marketType` varchar(15) NOT NULL default '',
  `instrumentId` int(10) NOT NULL default '0',
  `instrumentName` varchar(15) NOT NULL default '',
  `scriptCode` int(10) NOT NULL default '0',
  `contractCode` varchar(20) NOT NULL default '',
  `scriptGroup` varchar(5) NOT NULL default '',
  `scriptType` varchar(5) NOT NULL default '',
  `expiryDate` date NOT NULL default '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL default '',
  `strikePrice` float NOT NULL default '0',
  `optionType` varchar(4) NOT NULL default '',
  `previousClosePrice` float NOT NULL default '0',
  `openPrice` float NOT NULL default '0',
  `highPrice` float NOT NULL default '0',
  `lowPrice` float NOT NULL default '0',
  `closePrice` float NOT NULL default '0',
  `totalQtyTrade` int(10) NOT NULL default '0',
  `totalValueTrade` double NOT NULL default '0',
  `lifeHigh` float NOT NULL default '0',
  `lifeLow` float NOT NULL default '0',
  `quoteUnits` varchar(10) NOT NULL default '',
  `settlementPrice` float NOT NULL default '0',
  `noOfTrades` int(6) NOT NULL default '0',
  `openInterest` double NOT NULL default '0',
  `avgTradePrice` float NOT NULL default '0',
  `tdcl` float NOT NULL default '0',
  `lstTradePrice` float NOT NULL default '0',
  `remarks` text NOT NULL,
  PRIMARY KEY  (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bhavcopy`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) NOT NULL default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) NOT NULL default '',
  `dwAmount` double default '0',
  `plStatus` char(2) NOT NULL default '',
  `plAmount` double default '0',
  `transactionDate` date default NULL,
  `transType` varchar(20) default NULL,
  `transMode` varchar(30) character set utf8 NOT NULL default '',
  `exchange` varchar(20) character set utf8 default NULL,
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `passwd` varchar(30) character set utf8 default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `oneSideBrok` float default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `remiser` int(6) default '0',
  `remiserBrok` float default '0',
  `remiserBrokIn` tinyint(1) default '1',
  `clientBroker` int(2) default NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `client`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(50) default NULL,
  `exchange` varchar(30) character set utf8 default NULL,
  `oneSideBrok` float default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientexchange`
-- 

CREATE TABLE `clientexchange` (
  `clientexchangeId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `exchange` varchar(50) default NULL,
  `brok` int(50) default NULL,
  PRIMARY KEY  (`clientexchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientexchange`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `exchange`
-- 

CREATE TABLE `exchange` (
  `exchangeId` int(6) unsigned NOT NULL auto_increment,
  `exchange` varchar(20) character set utf8 default NULL,
  `multiply` tinyint(1) NOT NULL default '0',
  `profitBankRate` float default NULL,
  `lossBankRate` float default NULL,
  PRIMARY KEY  (`exchangeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `exchange`
-- 

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES 
(1, 'CX', 0, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `expensemaster`
-- 

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL auto_increment,
  `expenseName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`expensemasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `expensemaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- 
-- Dumping data for table `expiry`
-- 

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES 
(24, 'CxGold', '01DEC2010', 'CX'),
(25, 'CxSilver', '01DEC2010', 'CX'),
(26, 'CxCrude', '01DEC2010', 'CX'),
(27, 'CxEuro', '01DEC2010', 'CX'),
(29, 'GBP', '01DEC2010', 'CX'),
(30, 'CxNatu', '01DEC2010', 'CX');

-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `general`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` float default NULL,
  `brok2` float default NULL,
  `oneSideBrok` float default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchangeId` int(6) unsigned default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `multiply` float default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchangeId`, `exchange`, `multiply`) VALUES 
('CxGold', 'CxGold', 'CxGold', 40, 40, 40, 1, 1, 100, 400, 520, NULL, NULL, 'CX', NULL),
('CxSilver', 'CxSilver', 'CxSilver', 40, 40, 40, 1, 1, 50, 670, 820, NULL, NULL, 'CX', NULL),
('CxCrude', 'CxCrude', 'CxCrude', 40, 40, 40, 1, 1, 500, 45, 85, NULL, NULL, 'CX', NULL),
('CxEuro', 'CxEuro', 'CxEuro', 40, 40, 40, 1, 1, 125000, 0.5, 3, NULL, NULL, 'CX', NULL),
('CxNatu', 'CxNatu', 'CxNatu', 0, 0, 35, 1, 1, 10, 0, 0, NULL, NULL, 'CX', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `menu`
-- 

CREATE TABLE `menu` (
  `menuId` int(10) unsigned NOT NULL auto_increment,
  `fileToOpen` varchar(200) character set utf8 NOT NULL,
  `title` varchar(55) character set utf8 NOT NULL,
  `displayToAdmin` tinyint(1) unsigned NOT NULL default '0',
  `displayToOperator` tinyint(1) unsigned NOT NULL default '0',
  `displayToClient` tinyint(1) unsigned NOT NULL default '0',
  `newWindow` tinyint(1) unsigned NOT NULL default '0',
  `newWindowName` char(20) NOT NULL,
  `newWindowPerameter` text character set utf8 NOT NULL,
  PRIMARY KEY  (`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `menu`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `newexpmaster`
-- 

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL auto_increment,
  `newExpName` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `newexpmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `clientId2` int(6) default NULL,
  `firstName2` varchar(35) character set utf8 default NULL,
  `middleName2` varchar(35) character set utf8 default NULL,
  `lastName2` varchar(35) character set utf8 default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `price2` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  `refOrderId` int(11) default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `otherexp`
-- 

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL auto_increment,
  `otherExpName` varchar(50) character set utf8 default NULL,
  `otherExpDate` date default NULL,
  `otherExpAmount` float default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherExpMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `otherexp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `partybrokerage`
-- 

CREATE TABLE `partybrokerage` (
  `partybrokerageId` int(11) NOT NULL auto_increment,
  `partyId` int(11) default NULL,
  `brokerageDate` date default NULL,
  `brokerage` float NOT NULL,
  PRIMARY KEY  (`partybrokerageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `partybrokerage`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `settingsKey` varchar(30) character set utf8 default NULL,
  `settingsValue` varchar(60) default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `settings`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  `exchange` varchar(20) character set utf8 default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `standing`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `storedbhav`
-- 

CREATE TABLE `storedbhav` (
  `stordId` int(11) NOT NULL,
  `storDate` varchar(20) character set utf8 NOT NULL,
  `status` varchar(10) character set utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `storedbhav`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `clientId2` int(6) default NULL,
  `firstName2` varchar(35) character set utf8 default NULL,
  `middleName2` varchar(35) character set utf8 default NULL,
  `lastName2` varchar(35) character set utf8 default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `price2` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default '0',
  `exchange` varchar(20) character set utf8 default NULL,
  `refTradeId` int(6) default NULL,
  `selfRefId` int(6) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxt`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtv1`
-- 

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtv1`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `clientId` varchar(50) NOT NULL default '',
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL default '',
  `deletepassword` varchar(50) NOT NULL default '',
  `userType` varchar(20) character set utf8 default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `user`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendor`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `itemId` varchar(50) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendorbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  `clientId2` int(6) NOT NULL,
  `firstName2` varchar(35) character set utf8 NOT NULL,
  `middleName2` varchar(35) character set utf8 NOT NULL,
  `lastName2` varchar(35) character set utf8 NOT NULL,
  `exchange` varchar(20) character set utf8 NOT NULL,
  `refTradeId` int(6) NOT NULL,
  `selfRefId` int(6) NOT NULL,
  `price2` float default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

