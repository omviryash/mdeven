<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
	<a href="./index.php">Home</a>
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD colspan="5" align="center">Name</TD>
  <TD colspan="2" align="center">&nbsp;</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD colspan="2" align="center">NetProfitLoss</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="5">
      {$trades[sec1].clientId} : {$trades[sec1].clientName}
    </TD>
    <TD colspan="2">&nbsp;</TD>
    <TD align="right">
      {$trades[sec1].clientTotProfitLoss}
    </TD>
    <TD colspan="2" align="right">
      {$trades[sec1].clientTotBrok}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss < 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss >= 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
  </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="12">&nbsp;</TD></TR>
<TR>
  <TD align="center">B-S</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD colspan="2" align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].profitLoss}</TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}
      {$wholeItemArr[sec2].netProfitLoss}
    {else}
      &nbsp;
    {/if}
  </TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss >= 0}
      {$wholeItemArr[sec2].netProfitLoss}
    {else}
      &nbsp;
    {/if}
  </TD>
</TR>
{/section}
<TR>
  <TD align="right" colspan="10">&nbsp;</TD>
  <TD align="center" NOWRAP>{$wholeNetLossOnly}</TD>
  <TD align="center" NOWRAP>{$wholeNetProfitOnly}</TD>
</TR>
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>{$wholeProfitLoss}</TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD colspan="2" align="center" NOWRAP>{$wholeNetProfitLoss}</TD>
</TR>
</TABLE>
</BODY>
</HTML>
