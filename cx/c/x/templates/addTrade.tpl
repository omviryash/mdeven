<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type='text/javascript' src="./js/jquery.autocomplete.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/jquery.autocomplete.css" />
  {literal}
  <style type="text/css">
    span  {
      color:white;
    }
    a  {
      color:white;
    }
    div#storeOrPendig1  {
      float:left;
      display:inline;
      width:49%;
      border:1px solid red;
    }
    div#storeOrPendig2  {
      float:right;
      display:inline;
      width:49%;
      border:1px dotted red;
    }
  </style>
  {/literal}
  <title>!! MCX !! Add Trade</title>
</head>
<body {if $buyOrSell == "Buy"} bgColor="blue" {else if $buyOrSell == "Sell"} bgColor="red" {/if} id="body" >
<form name="form1" action="{$PHP_SELF}" >
<input type="hidden" name="tradeHidden" value="trade" />
  <span><a href="index.php">Home</a> </span>
  {if $clientIdAskInTextBox == 1}
  [ <span>Client Id 1: </span  ><span id="clientIdDiv" style="color:yellow;font-weight:bold;"></span> ]
  {if $twoClientTextBox == 1}
  [ <span>Client Id 2: </span  ><span id="clientIdDiv2" style="color:yellow;font-weight:bold;"></span> ]
  {/if}
  <br />
  {/if}
  {if $askTime == 1}
    {html_select_time prefix="timeHidden" use_24_hours=true time=$trade.tradeTime}
  {else}
    <input type="hidden" name="timeHidden" />
  {/if}
  {html_select_date start_year="-1" day_value_format="%02d" month_value_format="%m" day_format="%d" month_format="%m" field_order="DMY" time=$trade.tradeDate }
  <span>Client 1:</span>
  {if $clientIdAskInTextBox == 1}
  <input type="text" name="clientId" id="clientId" size=4 value="{$trade.clientId}" />
  {else}
  <select name="clientId" id="clientId" >
    {html_options values=$clientIdValues output=$clientIdOutput selected=$trade.clientId}
  </select>
  {/if}
  {if $twoClientTextBox == 1}
  <span>Client 2:</span>
  {if $clientIdAskInTextBox == 1}
  <input type="text" name="clientId2" id="clientId2" size=4 value="{$defaultClientId2}" />
  {else}
  <select name="clientId2" id="clientId2" >
    {html_options values=$clientId2Values output=$clientId2Output selected=$defaultClientId2}
  </select>
  {/if}
  {else}
    <input type="hidden" name="clientId2" id="clientId2" size=4 value="{$defaultClientId2}" />
  {/if}
  <span>Price 1: <input type="text" name="price1" id="price1" size="6" onkeydown="changePrice(this,event);" value="{$trade.price}" /></span>
  {if $twoClientTextBox == 1}
  <span>Price 2: <input type="text" name="price2" id="price2" size="6" onkeydown="changePrice(this,event);" value="{$trade.price2}"/></span><br />
  {else}
  <input type="hidden" name="price2" id="price2" /><br />
  {/if}
  <input type="text" name="buySell" id="buySell" value="{$buyOrSell}" disabled size="10"/>
  <input type="hidden" name="buySellHidden" id="buySellHidden" value="{$buyOrSell}" />
  {if $exchange == "MCX" || $exchange == "CX" }
  <select name="itemId" onchange="itemChange(this);">
    {html_options values=$item.itemId output=$item.item selected=$selectedItem}
  </select>
  {else}
  <input type="text" name="itemId" id="itemId" onkeydown="itemChange(this);" value="{$selectedItem}" />
  {/if}
  <select name="expiryDate" id="expiryDate">
  </select> &nbsp;
  <span>Qty.by Lot : <input type="text" name="lot" id="lot" size="3" value="{$lot}" onkeydown="changePrice(this,event);" /> &nbsp;</span>
  <span>Quantity : <input type="text" name="quantity" id="quantity" size="5" disabled  /></span>
  <input type="hidden" name="quantityHidden" id="quantityHidden" />
  <input type="hidden" name="min" id="min" />
  <input type="hidden" name="exchange" id="exchange" value="{$exchange}" />
  <input type="hidden" name="tradeId" id="tradeId" value="{$tradeId}" />
  <input type="hidden" name="refTradeId" id="refTradeId" value="{$refTradeId}" />
  {if $forStand == 1}
    <select name="standing">
      <option name="open" value="-1">Open Standing</option>
      <option name="close" value="1">Close Standing</option>
    </select>
  {else}
    <input type="hidden" name="standing" value="0" />
  {/if}
  <input type="button" name="button1" id="trade" value="Trade" />
  <span><a href="javascript:void(0);" style="color:white;" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Ctrl + Enter => Trade)" >Help</a></span>
<div id="storeOrPendig1"></div>
<div id="storeOrPendig2"></div>
</form>
<script type="text/javascript">
  var item = new Array();
  var rangeStart = new Array();
  var rangeEnd = new Array();
  {foreach from=$item.item item=itemName key=id}
  item[{$id}] = '{$itemName}';
  {/foreach}
  {foreach from=$item.rangeStart item=rangeStart key=rangeStartId}
  rangeStart[{$rangeStartId}] = {$rangeStart};
  {/foreach}
  {foreach from=$item.rangeEnd item=rangeEnd key=rangeEndId}
  rangeEnd[{$rangeEndId}] = {$rangeEnd};
  {/foreach}

  {literal}
  $(document).ready(function()
  {
    holdingArray = new Array();
    var holdingArrayLength = holdingArray.length;

    var now = new Date();
    var hour = now.getHours();
    if (hour < 10)
      hour = '0'+hour;
    var minute = now.getMinutes();
    if (minute < 10)
      minute = '0'+minute;
    var second = now.getSeconds();
    if (second < 10)
      second = '0'+second;
    var sTime = hour + ":" + minute + ":" + second;
    //document.form1.timeHidden.value= sTime;
    $("#trade").click(function()
    {
      var confirmMessage;
      if(document.form1.tradeHidden.value == "trade")
        confirmMessage = "Are you sure to Trade?";
      if(document.form1.tradeHidden.value == "RL")
        confirmMessage = "Are you sure to RL?";
      if(document.form1.tradeHidden.value == "SL")
        confirmMessage = "Are you sure to SL?";
      if(confirm(confirmMessage))
      {
        var storeOrPendigInnerHtml1 = "";
        var storeOrPendigInnerHtml2 = "";
        {/literal}
        {if $askTime != 1}
        var now = new Date();
        var hour = now.getHours();
        if (hour < 10)
          hour = '0'+hour;
        var minute = now.getMinutes();
        if (minute < 10)
          minute = '0'+minute;
        var second = now.getSeconds();
        if (second < 10)
          second = '0'+second;
        var sTime = hour + ":" + minute + ":" + second;
        document.form1.timeHidden.value = sTime;
        {else}
        sTime = document.form1.timeHiddenHour.value + ":" + document.form1.timeHiddenMinute.value + ":" + document.form1.timeHiddenSecond.value;
        {/if}
        {literal}
        holdingArray[holdingArrayLength] = new Array();
        holdingArray[holdingArrayLength]['dateObjectArray'] = document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value +" "+ sTime;
        {/literal}
        {if $clientIdAskInTextBox != 1}
        holdingArray[holdingArrayLength]['clientIdArray']   = document.form1.clientId.options[document.form1.clientId.selectedIndex].text;
        {if $twoClientTextBox == 1}
        holdingArray[holdingArrayLength]['clientId2Array']  = document.form1.clientId2.options[document.form1.clientId2.selectedIndex].text;
        {else}
        holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
        {/if}
        {else}
        holdingArray[holdingArrayLength]['clientIdArray']   = document.getElementById("clientIdDiv").innerHTML;
        {if $twoClientTextBox == 1}
        holdingArray[holdingArrayLength]['clientId2Array']  = document.getElementById("clientIdDiv2").innerHTML;
        {else}
        holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
        {/if}
        {/if}
        holdingArray[holdingArrayLength]['price1Array']     = document.form1.price1.value;
        holdingArray[holdingArrayLength]['price2Array']     = document.form1.price2.value;
        holdingArray[holdingArrayLength]['buySellHidden']   = document.form1.buySellHidden.value;
        {if $exchange == "MCX" || $exchange == "CX" }
        holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.options[document.form1.itemId.selectedIndex].text;
        {else}
        holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.value;
        {/if}
          {literal}
        holdingArray[holdingArrayLength]['tradeHiddenArray']= document.form1.tradeHidden.value
        holdingArray[holdingArrayLength]['elementNoArray']  = holdingArray.length;
        holdingArray[holdingArrayLength]['status']          = "Pending";
        holdingArrayLength +=1;
        if(document.form1.tradeId.value == 0)
          toDo = "Insert";
        else
          toDo = "Update";

        $.ajax(
        {
          type:"POST",{/literal}
          url :'saveRecord.php',{literal}
          data:
          {
            dateObject    :document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value,
            clientId      :document.form1.clientId.value,
            clientId2     :document.form1.clientId2.value,
            price1        :document.form1.price1.value,
            price2        :document.form1.price2.value,
            itemId        :document.form1.itemId.value,
            buySellHidden :document.form1.buySellHidden.value,
            expiryDate    :document.form1.expiryDate.value,
            standing      :document.form1.standing.value,
            quantityHidden:document.form1.quantityHidden.value,
            exchange      :document.form1.exchange.value,
            trade         :document.form1.tradeHidden.value,
            tradeId       :document.form1.tradeId.value,
            refTradeId    :document.form1.refTradeId.value,
            selfRefId     :0,
            sTime         :sTime,
            toDo          :toDo,
            status        :"Pending",
            elementNo     :(holdingArray.length)-1
          },
          success: function(response)
          {
            var handleResponce = response.split(",");
            if(handleResponce[0] == "Stored")
              holdingArray[handleResponce[1]]['status'] = "Stored";
            else if(handleResponce[0] == "Pending")
              holdingArray[handleResponce[1]]['status'] = "Pending";
            var storeOrPendigInnerHtml1 = "";
            var storeOrPendigInnerHtml2 = "";
            for(i = (holdingArray.length - 1) ; i >= 0 ; i--)
            {
              if(holdingArray[i]['status'] != "Stored")
              {
                storeOrPendigInnerHtml1 += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['dateObjectArray']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['clientIdArray']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['buySellHidden']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['clientId2Array']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['price1Array']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['price2Array']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['itemIdArray']+"&nbsp;";
                storeOrPendigInnerHtml1 += holdingArray[i]['status']+"<hr />";
              }
              else
              {
                storeOrPendigInnerHtml2 += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['dateObjectArray']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['clientIdArray']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['buySellHidden']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['clientId2Array']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['price1Array']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['price2Array']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['itemIdArray']+"&nbsp;";
                storeOrPendigInnerHtml2 += holdingArray[i]['status']+"<hr />";
              }
            }
            $("#storeOrPendig1").html(storeOrPendigInnerHtml1);
            $("#storeOrPendig2").html(storeOrPendigInnerHtml2);
            $("#clientId").focus();
            $("#clientId").select();
            $("#lot").val(1);
          }
        });
        for(i = (holdingArray.length - 1) ; i >= 0 ; i--)
        {
          if(holdingArray[i]['status'] != "Stored")
          {
            storeOrPendigInnerHtml1 += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['dateObjectArray']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['clientIdArray']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['buySellHidden']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['clientId2Array']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['price1Array']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['price2Array']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['itemIdArray']+"&nbsp;";
            storeOrPendigInnerHtml1 += holdingArray[i]['status']+"<hr />";
          }
          else
          {
            storeOrPendigInnerHtml2 += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['dateObjectArray']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['clientIdArray']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['buySellHidden']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['clientId2Array']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['price1Array']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['price2Array']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['itemIdArray']+"&nbsp;";
            storeOrPendigInnerHtml2 += holdingArray[i]['status']+"<hr />";
          }
        }
        $("#storeOrPendig1").html(storeOrPendigInnerHtml1);
        $("#storeOrPendig2").html(storeOrPendigInnerHtml2);
        {/literal}
        {if $isEdit == 1}
        location.href = "{$goTo}.php";
        {/if}
        {literal}
      }
    });
    $("#clientId").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv").innerHTML == "")
        $("#clientIdDiv").html("Client not available");
      {/if}
      {literal}
    });
    $("#clientId2").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv2").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv2").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv2").innerHTML == "")
        $("#clientIdDiv2").html("Client not available");
      {/if}
      {literal}
    });
    $("#itemId").autocomplete(item,
    {
      minChars: 0,
      max: 30,
      autoFill: true,
      mustMatch: true,
      matchContains: false,
      scrollHeight: 220,
      formatItem: function(data, i, total)
      {
        return data[0];
      }
    });
    $("#lot").keydown(function()
    {
      minQuantityFunc(this.value);
    });
    $("#price1").blur(function()
    {
      {/literal}
      {if $isEdit == 0}
        $("#price2").val(this.value);
      {section name=sec loop=$item.rangeStart}
      {if $item.rangeStart[sec] != $item.rangeEnd[sec]}
      {if $item.rangeStart[sec] > 0 && $item.rangeEnd[sec] > 0}
      if((document.form1.price1.value >= {$item.rangeStart[sec]}) && (document.form1.price1.value <= {$item.rangeEnd[sec]})){literal}
      {
        {/literal}
        document.form1.min.value = {$item.min[sec]};
        {if $exchange == "MCX" || $exchange == "CX" }
        selectOptionByValue(document.form1.itemId, "{$item.itemId[sec]}");
        minQuantityFunc(document.form1.lot.value);
        {/if}
        {literal}
      }
      {/literal}
      {/if}
      {/if}
      {/section}
      {/if}
      {literal}
    });
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      {/literal}
      {if $exchange == "MCX" || $exchange == "CX" }
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {literal}
      else if(e.ctrlKey && code == 49)
      {
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
        return false;
      }
      {/literal}
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {else if $exchange == "F_O"}
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {/if}
      {literal}
      if(e.ctrlKey && e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "SL";
        $("#trade").click();
        return false;
      }
      if(e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "RL";
        $("#trade").click();
        return false;
      }
      {/literal}
      {if $cfgStoreOnEnter == 1}
      {literal}
      if(code == 13)
      {
        document.form1.tradeHidden.value = "trade";
        $("#trade").click();
        return false;
      }
      {/literal}
      {else if $cfgStoreOnEnter != 1}
      {literal}
      if(e.ctrlKey && code == 13)
      {
        document.form1.tradeHidden.value = "trade";
        $("#trade").click();
        return false;
      }
      if(code == 13)
      {
        window.event.keyCode = 9;
      }
      {/literal}
      {/if}
      {literal}
      if(code == 109 || code == 123)
      {
        $("#body").css("background-color","red");
        $("#buySell").val("Sell");
        $("#buySellHidden").val("Sell");
        return false;
      }
      if(code == 107 || code == 120)
      {
        $("#body").css("background-color","blue");
        $("#buySell").val("Buy");
        $("#buySellHidden").val("Buy");
        return false;
      }
    });

  });
  function changePrice(theObject,event)
  {
    var changePriceCode = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    if(changePriceCode == 38)
      theObject.value = parseInt(theObject.value)+1;
    if(changePriceCode == 40)
      theObject.value = parseInt(theObject.value)-1;
    if(theObject.name == "lot")
      plusOrMinusValue = 5;
    else
      plusOrMinusValue = 10;
    if(changePriceCode == 33)
      theObject.value = parseInt(theObject.value)+plusOrMinusValue;
    if(changePriceCode == 34)
      theObject.value = parseInt(theObject.value)-plusOrMinusValue;
  }
  function minQuantityFunc(lot)
  {
    document.form1.quantity.value = lot*document.form1.min.value;
    document.form1.quantityHidden.value = lot*document.form1.min.value;
  }
  function itemChange(theObject)
  {
    var form   = theObject.form;
    itemId     = document.form1.itemId;
    expiryDate = document.form1.expiryDate;
    expiryDate.options.length = 0;
    {/literal}
    {if $exchange == "MCX" || $exchange == "CX" }
      {section name="itemSec" loop=$item.itemId}
      if(itemId.selectedIndex == {%itemSec.index%})
      {literal}
      {
        {/literal}
        {section name="expiryDateSec" loop=$expiryDate[itemSec]}
        expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[itemSec][expiryDateSec]}","{$expiryDate[itemSec][expiryDateSec]}");
        expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
        {/section}
        {literal}
      }
      {/literal}
      {/section}
    {else if $exchange == "F_O"}
      {section name="expiryDateSec" loop=$expiryDate}
      expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[expiryDateSec]}","{$expiryDate[expiryDateSec]}");
      expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
      {/section}
    {/if}

    {section name=sec loop=$item.itemId}
    if((document.form1.itemId.value == "{$item.itemId[sec]}")){literal}
    {
      {/literal}
      document.form1.min.value = {$item.min[sec]};
      minQuantityFunc(document.form1.lot.value);
      {literal}
    }
    {/literal}
    {/section}
    {literal}
  }
  function selectOptionByValue(selObj, val)
  {
    var A = selObj.options, L = A.length;
    while(L)
    {
      if (A[--L].value == val)
      {
        selObj.selectedIndex = L;
        L = 0;
        break;
      }
    }
    itemChange(document.form1.itemId);
  }
  {/literal}
  $('#clientId').focus();
  itemChange(document.form1.itemId);

</script>
</body>
</html>