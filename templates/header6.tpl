<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brokers</A></LI>
    <LI><A href="bankMasterList.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expense</A></LI>
    <LI><A href="exchangeAdd.php">Exchange</A></LI>
    <LI><A href="itemRates.php">Item Rates</A></LI>
    <LI><A href="highLowView.php">High / Low</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A>
  <UL>
    <LI><A href="#">Mcx Trades</A>
    	<UL>
    	  <LI><A href="clientTradesMcx.php"                 >Client Trades</A></LI>
          <LI><A href="clientTradesMcx-2.php"                 >Client Trades Dynamic</A></LI>
		    <LI><A href="brokerTradesMcx.php"               >Broker Trades</A></LI>
    	  <LI><A href="../mdevencx/clientTrades.php">Client Trades Comex</A></LI>
    	  <LI><A href="clientTradesMcx.php?display=detailed">Client Detailed Trades</A></LI>
		    <LI><A href="brokerTradesMcx.php?display=detailed">Broker Detailed Trades</A></LI>
    	  <LI><A href="clientTradesPer1sideMcx.php"         >Client Trades in % 1 Side</A></LI>
    	  <LI><A href="brokerTradesPer1sideMcx.php"         >Broker Trades in % 1 Side</A></LI>
    	  <LI><A href="clientTradesPer2side2BothMcx.php"    >Client Trades in % 2 Side</A></LI>
    	  <LI><A href="brokerTradesPer2side2BothMcx.php"    >Broker Trades in % 2 Side</A></LI>
    	  <LI><A href="clientTradesMcx2side.php"            >Client Trades in Rs 2 Side</A></LI>
    	</UL>
    </LI>
    <LI><A href="selectDtSession.php?goTo=clientTrades">Detailed Trades</A></LI>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="ajxStoreLimitTradeMcx1.php">Data Entry Mcx Fast</A></LI>
    <LI><A href="#">&nbsp;</A></LI>
    <LI><A href="" onClick="tradeWindow=window.open('../mdevencx/addTrade.php?exchange=CX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry Comex</A></LI>
    <?php
      if($tradeInNewWindow == 1)
      {
    ?>
        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry Mcx ClientId</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry MCX New</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTradeNew.php?exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry MCX Aagad</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=MCX&twoClientTextBox=0', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry MCX New</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTradeHighLow.php?exchange=MCX', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=850, height=165, left=1, top=300'); return false;">Data Entry MCX High/Low</A></LI>
    <?php
      }
      else
      {
    ?>
        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=MCX">Data Entry Mcx ClientId</A></LI>
        <LI><A href="addTrade.php?exchange=MCX">Data Entry MCX New</A></LI>
        <LI><A href="addTradeHighLow.php?exchange=MCX">Data Entry MCX New</A></LI>
    <?php
      }
    ?>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
    <LI><A href="bulkChange.php">Bulk Change</A></LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="mnuStand.php"                                   >Standing</A></LI>
    <LI><A href="clientTradesMcx.php?display=gross"              >Generate Bill MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX</A></LI>
    <LI><A href="#">Rollback</A></LI>
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <LI><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWiseGross">Script Gross MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWisePL">Script wise PL</A></LI>
    <LI><A href="clientTradesMcx.php?display=gross">Bill Gross MCX Client</A></LI>
    <LI><A href="brokerTradesMcx.php?display=gross">Bill Gross MCX Broker</A></LI>
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="backup.php">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
    <LI><A href="#">ExtraMenu</A>
      <UL>
        <LI><A href="storeLimitTradeMcx.php">Data Entry Mcx Slow</A></LI>
      </UL>
    </LI>
  </UL>
</LI>

<LI><A href="selectDtSession.php">Date Range</A>
</LI>
<LI><A href="logout.php">Logout</A>
</LI>

</UL>
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>