<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type='text/javascript' src="./js/jquery.autocomplete.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/jquery.autocomplete.css" />
  {literal}
  <style type="text/css">
    span  {
      color:white;
    }
    a  {
      color:white;
    }
  </style>
  {/literal}
  <title>!! MCX !! Add Trade</title>
</head>
<body bgColor="blue" id="body" >
  <span><a href="index.php">Home</a> </span>
<form name="form1" action="{$PHP_SELF}" >
<input type="hidden" name="tradeHidden" value="trade" />
  {if $clientIdAskInTextBox == 1}
  [ <span>Client Id 1: </span  ><span id="clientIdDiv" style="color:yellow;font-weight:bold;"></span> ]
  {if $twoClientTextBox == 1}
  [ <span>Client Id 2: </span  ><span id="clientIdDiv2" style="color:yellow;font-weight:bold;"></span> ]
  {/if}
  <br />
  {/if}
  {if $askTime == 1}
    {html_select_time prefix="timeHidden" use_24_hours=true}
  {else}
    <input type="hidden" name="timeHidden" />
  {/if}
  {html_select_date start_year="-1" day_value_format="%02d" month_value_format="%m" day_format="%d" month_format="%m" field_order="DMY" }
  <span>Client 1:</span>
  {if $clientIdAskInTextBox == 1}
  <input type="text" name="clientId" id="clientId" size=4 />
  {else}
  <select name="clientId" id="clientId" >
    {html_options values=$clientIdValues output=$clientIdOutput}
  </select>
  {/if}
  {if $twoClientTextBox == 1}
  <span>Client 2:</span>
  {if $clientIdAskInTextBox == 1}
  <input type="text" name="clientId2" id="clientId2" size=4 value="{$defaultClientId2}" />
  {else}
  <select name="clientId2" id="clientId2" >
    {html_options values=$clientId2Values output=$clientId2Output selected=$defaultClientId2}
  </select>
  {/if}
  {else}
    <input type="hidden" name="clientId2" id="clientId2" size=4 value="{$defaultClientId2}" />
  {/if}
  <span>Price 1: <input type="text" name="price1" id="price1" size="6" onkeydown="changePrice(this,event);" /></span>
  {if $twoClientTextBox == 1}
  <span>Price 2: <input type="text" name="price2" id="price2" size="6" onkeydown="changePrice(this,event);" /></span><br />
  {else}
  <input type="hidden" name="price2" id="price2" /><br />
  {/if}
  <input type="text" name="buySell" id="buySell" value="{$buyOrSell}" disabled size="10"/>
  <input type="hidden" name="buySellHidden" id="buySellHidden" value="{$buyOrSell}" />
  {if $exchange == "MCX"}
  <select name="itemId" onchange="itemChange(this);">
    {html_options values=$item.itemId output=$item.item}
  </select>
  {else}
  <input type="text" name="itemId" id="itemId" onkeydown="itemChange(this);" />
  {/if}
  <select name="expiryDate" id="expiryDate">
  </select> &nbsp;
  <span>Qty.by Lot : <input type="text" name="lot" id="lot" value="1" size="3" onkeydown="changePrice(this,event);" /> &nbsp;</span>
  <span>Quantity : <input type="text" name="quantity" id="quantity" size="5" disabled /></span>
  <input type="hidden" name="quantityHidden" id="quantityHidden" />
  <input type="hidden" name="min" id="min" />
  <input type="hidden" name="exchange" id="exchange" value="{$exchange}" />
  {if $forStand == 1}
    <select name="standing">
      <option name="open" value="-1">Open Standing</option>
      <option name="close" value="1">Close Standing</option>
    </select>
  {else}
    <input type="hidden" name="standing" value="0" />
  {/if}
  <input type="button" name="button1" id="trade" value="Trade" />
  <span  ><a href="javascript:void(0);" style="color:white;" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Enter => Trade)" >Help</a></span>
</form>
  <div id="storeOrPendig"></div>
<script type="text/javascript">
  var item = new Array();
  {foreach from=$item.item item=itemName key=id}
    item[{$id}] = '{$itemName}';
  {/foreach}
  {literal}
  $(document).ready(function()
  {
    holdingArray = new Array();
    var holdingArrayLength = holdingArray.length;

    var now = new Date();
    var hour = now.getHours();
    if (hour < 10)
      hour = '0'+hour;
    var minute = now.getMinutes();
    if (minute < 10)
      minute = '0'+minute;
    var second = now.getSeconds();
    if (second < 10)
      second = '0'+second;
    var sTime = hour + ":" + minute + ":" + second;
    //document.form1.timeHidden.value= sTime;
    $("#trade").click(function()
    {
      ///////////////////////////// High low check : Start //////////

        $.ajax({
          url: "./highLowCheckAj.php",
          data: 
          {
            dateObject    :document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value,
            price1        :document.form1.price1.value,
            price2        :document.form1.price2.value,
            itemId        :document.form1.itemId.value,
          },
          success: function(data){
            if(data == "invalid")
            {
              alert("Price is out of High Low");
              setTimeout(function(){$("#price1").focus()}, 1);
            }        
            else
            {
              var confirmMessage;
              if(document.form1.tradeHidden.value == "trade")
                confirmMessage = "Are you sure to Trade?";
              if(document.form1.tradeHidden.value == "RL")
                confirmMessage = "Are you sure to RL?";
              if(document.form1.tradeHidden.value == "SL")
                confirmMessage = "Are you sure to SL?";
              if(confirm(confirmMessage))
              {
                var storeOrPendigInnerHtml = "";
                {/literal}
                {if $askTime != 1}
                var now = new Date();
                var hour = now.getHours();
                if (hour < 10)
                  hour = '0'+hour;
                var minute = now.getMinutes();
                if (minute < 10)
                  minute = '0'+minute;
                var second = now.getSeconds();
                if (second < 10)
                  second = '0'+second;
                var sTime = hour + ":" + minute + ":" + second;
                document.form1.timeHidden.value = sTime;
                {else}
                sTime = document.form1.timeHiddenHour.value + ":" + document.form1.timeHiddenMinute.value + ":" + document.form1.timeHiddenSecond.value;
                {/if}
                {literal}
                holdingArray[holdingArrayLength] = new Array();
                holdingArray[holdingArrayLength]['dateObjectArray'] = document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value +" "+ sTime;
                {/literal}
                {if $clientIdAskInTextBox != 1}
                holdingArray[holdingArrayLength]['clientIdArray']   = document.form1.clientId.options[document.form1.clientId.selectedIndex].text;
                {if $twoClientTextBox == 1}
                holdingArray[holdingArrayLength]['clientId2Array']  = document.form1.clientId2.options[document.form1.clientId2.selectedIndex].text;
                {else}
                holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
                {/if}
                {else}
                holdingArray[holdingArrayLength]['clientIdArray']   = document.getElementById("clientIdDiv").innerHTML;
                {if $twoClientTextBox == 1}
                holdingArray[holdingArrayLength]['clientId2Array']  = document.getElementById("clientIdDiv2").innerHTML;
                {else}
                holdingArray[holdingArrayLength]['clientId2Array']  = "Self";
                {/if}
                {/if}
                holdingArray[holdingArrayLength]['price1Array']     = document.form1.price1.value;
                holdingArray[holdingArrayLength]['price2Array']     = document.form1.price2.value;
                holdingArray[holdingArrayLength]['buySellHidden']   = document.form1.buySellHidden.value;
                {if $exchange == "MCX"}
                holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.options[document.form1.itemId.selectedIndex].text;
                {else}
                holdingArray[holdingArrayLength]['itemIdArray']     = document.form1.itemId.value;
                {/if}
                  {literal}
                holdingArray[holdingArrayLength]['tradeHiddenArray']= document.form1.tradeHidden.value
                holdingArray[holdingArrayLength]['elementNoArray']  = holdingArray.length;
                holdingArray[holdingArrayLength]['status']          = "Pending";
                holdingArrayLength +=1;
                $.ajax(
                {
                  type:"POST",{/literal}
                  url :'saveRecord.php',{literal}
                  data:
                  {
                    dateObject    :document.form1.Date_Year.value +"-"+ document.form1.Date_Month.value +"-"+ document.form1.Date_Day.value,
                    clientId      :document.form1.clientId.value,
                    clientId2     :document.form1.clientId2.value,
                    price1        :document.form1.price1.value,
                    price2        :document.form1.price2.value,
                    itemId        :document.form1.itemId.value,
                    buySellHidden :document.form1.buySellHidden.value,
                    expiryDate    :document.form1.expiryDate.value,
                    standing      :document.form1.standing.value,
                    quantityHidden:document.form1.quantityHidden.value,
                    exchange      :document.form1.exchange.value,
                    trade         :document.form1.tradeHidden.value,
                    refTradeId    :0,
                    selfRefId     :0,
                    sTime         :sTime,
                    status        :"Pending",
                    elementNo     :(holdingArray.length)-1
                  },
                  success: function(response)
                  {
                    var handleResponce = response.split(",");
                    if(handleResponce[0] == "Stored")
                      holdingArray[handleResponce[1]]['status'] = "Stored";
                    else if(handleResponce[0] == "Pending")
                      holdingArray[handleResponce[1]]['status'] = "Pending";
                    var storeOrPendigInnerHtml = "";
                    for(i = 0; i < holdingArray.length ; i++)
                    {
                      if(holdingArray[i]['status'] != "Stored")
                      {
                        storeOrPendigInnerHtml += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['dateObjectArray']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['clientIdArray']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['buySellHidden']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['clientId2Array']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['price1Array']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['price2Array']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['itemIdArray']+"&nbsp;";
                        storeOrPendigInnerHtml += holdingArray[i]['status']+"<br />";
                      }
                    }
                    $("#storeOrPendig").html(storeOrPendigInnerHtml);
                    $("#clientId").focus();
                    $("#lot").val(1);
        
                  }
                });
                for(i = 0; i < holdingArray.length ; i++)
                {
                  if(holdingArray[i]['status'] != "Stored")
                  {
                    storeOrPendigInnerHtml += holdingArray[i]['tradeHiddenArray']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['dateObjectArray']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['clientIdArray']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['buySellHidden']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['clientId2Array']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['price1Array']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['price2Array']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['itemIdArray']+"&nbsp;";
                    storeOrPendigInnerHtml += holdingArray[i]['status']+"<br />";
                  }
                }
                $("#storeOrPendig").html(storeOrPendigInnerHtml);
              }              
            }     
          }
        });  
      ///////////////////////////// High low check : Stop //////////      
    });
    $("#clientId").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv").innerHTML == "")
        $("#clientIdDiv").html("Client not available");
      {/if}
      {literal}
    });
    $("#clientId2").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv2").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv2").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv2").innerHTML == "")
        $("#clientIdDiv2").html("Client not available");
      {/if}
      {literal}
    });
    $("#itemId").autocomplete(item,
    {
      minChars: 0,
      max: 30,
      autoFill: true,
      mustMatch: true,
      matchContains: false,
      scrollHeight: 220,
      formatItem: function(data, i, total)
      {
        return data[0];
      }
    });
    $("#lot").keydown(function()
    {
      minQuantityFunc(this.value);
    });
    $("#price1").blur(function()
    {
      $("#price2").val(this.value);
      {/literal}
      {section name=sec loop=$item.rangeStart}
      if((document.form1.price1.value >= {$item.rangeStart[sec]}) && (document.form1.price1.value <= {$item.rangeEnd[sec]})){literal}
      {
        {/literal}
        document.form1.min.value = {$item.min[sec]};
        {if $exchange == "MCX"}
        selectOptionByValue(document.form1.itemId, "{$item.itemId[sec]}");
        minQuantityFunc(document.form1.lot.value);
        {/if}
        {literal}
      }
      {/literal}
      {/section}
      {literal}
    });
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      {/literal}
      {if $exchange == "MCX"}
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {else if $exchange == "F_O"}
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {/if}
      {literal}
      if(e.ctrlKey && e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "SL";
        $("#trade").click();
        return false;
      }
      if(e.shiftKey && code == 13)
      {
        document.form1.tradeHidden.value = "RL";
        $("#trade").click();
        return false;
      }
      if(e.ctrlKey && code == 13)
      {
        document.form1.tradeHidden.value = "trade";
        $("#trade").click();
        return false;
      }

      if(code == 109)
      {
        $("#body").css("background-color","red");
        $("#buySell").val("Sell");
        $("#buySellHidden").val("Sell");
        return false;
      }
      if(code == 107)
      {
        $("#body").css("background-color","blue");
        $("#buySell").val("Buy");
        $("#buySellHidden").val("Buy");
        return false;
      }
    });

  });
  function changePrice(theObject,event)
  {
    var changePriceCode = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    if(changePriceCode == 38)
      theObject.value = parseInt(theObject.value)+1;
    if(changePriceCode == 40)
      theObject.value = parseInt(theObject.value)-1;
    if(theObject.name == "lot")
      plusOrMinusValue = 5;
    else
      plusOrMinusValue = 10;
    if(changePriceCode == 33)
      theObject.value = parseInt(theObject.value)+plusOrMinusValue;
    if(changePriceCode == 34)
      theObject.value = parseInt(theObject.value)-plusOrMinusValue;
  }
  function minQuantityFunc(lot)
  {
    document.form1.quantity.value = lot*document.form1.min.value;
    document.form1.quantityHidden.value = lot*document.form1.min.value;
  }
  function itemChange(theObject)
  {
    var form   = theObject.form;
    itemId     = document.form1.itemId;
    expiryDate = document.form1.expiryDate;
    expiryDate.options.length = 0;
    {/literal}
    {if $exchange == "MCX"}
    {section name="itemSec" loop=$item.itemId}
    if(itemId.selectedIndex == {%itemSec.index%})
    {literal}
    {
      {/literal}
      {section name="expiryDateSec" loop=$expiryDate[itemSec]}
      expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[itemSec][expiryDateSec]}","{$expiryDate[itemSec][expiryDateSec]}");
      expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
      {/section}
      {literal}
    }
    {/literal}
    {/section}
    {else if $exchange == "F_O"}
    {section name="expiryDateSec" loop=$expiryDate}
    expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[expiryDateSec]}","{$expiryDate[expiryDateSec]}");
    expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
    {/section}
    {/if}
    {section name=sec loop=$item.itemId}
    if((document.form1.itemId.value == "{$item.itemId[sec]}")){literal}
    {
      {/literal}
      document.form1.min.value = {$item.min[sec]};
      minQuantityFunc(document.form1.lot.value);
      {literal}
    }
    {/literal}
    {/section}
    {literal}
  }
  function selectOptionByValue(selObj, val)
  {
    var A = selObj.options, L = A.length;
    while(L)
    {
      if (A[--L].value == val)
      {
        selObj.selectedIndex = L;
        L = 0;
        break;
      }
    }
    itemChange(document.form1.itemId);
  }
  {/literal}
  $('#clientId').focus();
  itemChange(document.form1.itemId);
</script>
</body>
</html>