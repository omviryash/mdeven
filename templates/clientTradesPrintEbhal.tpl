<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: NORMAL}
.lossStyle   {color: black}
.profitStyle {color: black}
{/literal}
</STYLE>  
</HEAD>
<BODY>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="80%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>
    {if $clientIdSelected == 0}
      Scrip : 
      <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
      </SELECT>
    {/if}
  </TD>
  <TD>
    {if $clientIdSelected == 0}
      Expiry : 
      <SELECT name="expiryDate" onChange="document.form1.submit();">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
      </SELECT>
    {/if}
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>
<TABLE border="0" cellPadding="4" cellSpacing="0">
<TR>
  <TD colspan="9" align="center"><B>Bill</B></TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="4">&nbsp;</TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Date</TD>
  <TD align="center">Scrip</TD>
  <TD align="center">Net Profit / Loss</TD>
  {if $display == "detailed"} 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  {/if}
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].clientId != $trades[sec1].prevClientId or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR>
    <TD colspan="5"><B><U>{$trades[sec1].clientId} : {$trades[sec1].clientName}
      : ({$trades[sec1].clientDeposit})</U></B></TD>
    <TD colspan="4" align="center">{$trades[sec1].itemId}</TD>
  </TR>
{/if}
<TR style="color:{$trades[sec1].fontColor}">
  <TD align="center">{$trades[sec1].buySell}</TD>
  <TD align="right">{$trades[sec1].buyQty}</TD>
  <TD align="right">{$trades[sec1].price}</TD>
  <TD align="right">{$trades[sec1].sellQty}</TD>
  <TD align="right">{$trades[sec1].sellPrice}</TD>
  <TD>{if $trades[sec1].standing == "Close"}C/F{elseif $trades[sec1].standing == "Open"}B/F{/if}</TD>
  <TD NOWRAP>{$trades[sec1].tradeDate}
    {if $display == "detailed"} {$trades[sec1].tradeTime} {/if}
  </TD>
  <TD align="center" NOWRAP>&nbsp;</TD>
  <TD>&nbsp;</TD>
  {if $display == "detailed"} 
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
  {/if}
</TR>
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD align="right" NOWRAP>
      Net : {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="3" align="right" NOWRAP>
      &nbsp;&nbsp;&nbsp;</TD>
    <TD align="right" NOWRAP>
      {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      Net : {$trades[sec1].netProfitLoss}</FONT>
    </TD>
  {else}
    <TD colspan="4">{$trades[sec1].itemId} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR><TD colspan="9"><HR></TD></TR>
  <TR>
    <TD colspan="5">
      <U>: Total : </U>
    </TD>
    <TD colspan="3">
      &nbsp;&nbsp;&nbsp;
    </TD>
    <TD align="right"> = &nbsp;&nbsp;&nbsp;<B><U>
      {if $trades[sec1].clientTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].clientTotNetProfitLoss}</FONT></B></U></TD>
  </TR>
  {/if}
{/if}
{/section}
<TR>
  <TD colspan="9">Note - 10% Margin required on total value of open position (C/F).</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
