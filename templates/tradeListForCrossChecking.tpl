<HTML>
<HEAD>
  <TITLE>Trades</TITLE>
{literal}
<SCRIPT name="javascript">
function setValue()
{
  document.form1.doneValue.value="1";
} 
function conf(tradeIdSelected)
{
	if(confirm("Are You Sure You want to Delete Record?"))
			location.href="tradeDelete.php?tradeId="+tradeIdSelected;
}
function chkAll(checkid, exby) 
{
	if(typeof(checkid.length) == 'undefined')
	  checkid.checked = exby.checked? true:false
	else
	{
	  for (i = 0; i < checkid.length; i++)
	  {
	    checkid[i].checked = exby.checked? true:false
	  }
	}
}
function change()
{
 var select1value= document.form1.cboItem.value;
 var select2value=document.form1.cboExpiry;
 select2value.options.length=0;
{/literal}
  {section name=sec1 loop=$j}
   if( select1value=="{$itemId[sec1]}")
 	{literal}{{/literal}
 		{section name=sec2 loop=$expiryDate[sec1]}
       {if $expiryDate[sec1][sec2] eq $SExpiry}
         select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}","{$expiryDate[sec1][sec2]}"); 
         select2value.options[{$smarty.section.sec2.index}].selected=true;
       {else}
         select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}","{$expiryDate[sec1][sec2]}"); 
         select2value.options[{$smarty.section.sec2.index}].selected=false;
       {/if}
     {/section}
   {literal}}{/literal}
 {/section}
  {if $dataPosted == 0}
  document.form1.submit();
  {/if}
}

{literal}
function selectClient()
{
  var select1value= document.form1.cboClientBroker.value;
  var select2value=document.form1.cboClient;
  select2value.options.length=0;
  {/literal}
    select2value.options[0]=new Option("All","All"); 
    if(select1value==-1)
    	{literal}{{/literal}
    {section name=sec1 loop=$allCount}
        {if $clientIdValuesForAll[sec1] eq $SClientId}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$allClient[sec1]}","{$clientIdValuesForAll[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=true;
        {else}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$allClient[sec1]}","{$clientIdValuesForAll[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=false;
        {/if}
    {/section}
   {literal}}{/literal}
    
     if(select1value==0)
    	{literal}{{/literal}
    {section name=sec1 loop=$clientCount}
        {if $clientIdValuesForClient[sec1] eq $SClientId}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$clientOption[sec1]}","{$clientIdValuesForClient[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=true;
        {else}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$clientOption[sec1]}","{$clientIdValuesForClient[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=false;
        {/if}
    {/section}
   {literal}}{/literal}
    
     if(select1value==1)
    	{literal}{{/literal}
    {section name=sec1 loop=$brokerCount}
        {if $clientIdValuesForBroker[sec1] eq $SClientId}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$brokerOption[sec1]}","{$clientIdValuesForBroker[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=true;
        {else}
          select2value.options[{$smarty.section.sec1.index}+1]=new Option("{$brokerOption[sec1]}","{$clientIdValuesForBroker[sec1]}"); 
          select2value.options[{$smarty.section.sec1.index}+1].selected=false;
        {/if}
    {/section}
   {literal}}{/literal}
  {literal}
}
{/literal}
</SCRIPT>
</HEAD>
<A href="./index.php">Home</A>
<BODY onLoad="change();">
<FORM name="form1" method="POST">
<input type="hidden" name="doneValue" value=0>
<input type="hidden" name="dataPosted" value="{$dataPosted}" />

  Exchange:
  <SELECT name="exchange" onchange="document.form1.submit();" >
    <option value="All">All</option>
    {html_options selected="$exchangeSelected" values="$exchangeValues" output="$exchangeOutput" }
  </SELECT>
  
  Client/Broker:
  <SELECT name="cboClientBroker" onChange="selectClient();">
    <OPTION value="-1" {$cboClientBrokerAllSelected}>All</OPTION>
    <OPTION value="0" {$cboClientBrokerClientSelected}>Client</OPTION>
    <OPTION value="1" {$cboClientBrokerBrokerSelected}>Broker</OPTION>
  </SELECT>

  Client:
  <SELECT name="cboClient">
    <option value="All">All</option>
  </SELECT>
  
  Item:
  <SELECT name="cboItem" onChange="change();">
    <option value="All">All</option>
    {html_options values="$itemId" output="$itemId" selected=$SItem}
  </SELECT>
  
  ExpiryDate:
  <SELECT name="cboExpiry">
    <option value={$SExpiry} selected=selected>{$SExpiry}</option>
  </SELECT>
  
  <SELECT name="cboBuySell">
    {if $SBuySell eq "All" || $SBuySell eq ""}
    <option value="All" selected=selected>All</option>
    <option value="Buy">Buy</option>
    <option value="Sell">Sell</option>
    {elseif $SBuySell eq "Buy"}
    <option value="All">All</option>
    <option value="Buy" selected=selected>Buy</option>
    <option value="Sell">Sell</option>
    {else}
    <option value="All">All</option>
    <option value="Buy">Buy</option>
    <option value="Sell" selected=selected>Sell</option>
    {/if}
  </SELECT>
  <BR>
  From : {html_select_date prefix="from" time="$SFromDate" month_format=%b start_year=+2 end_year=-2 field_order="dmy"}
  To : {html_select_date prefix="to"  time="$SToDate" month_format=%b start_year=+2 end_year=-2 field_order="dmy"}
  &nbsp;Order By : 
  <SELECT name="orderBy" >
    <option value="orderByDefault" {if $selectedValue eq 1 } SELECTED {/if}>Order By Item</option>
    <option value="orderByTime" {if $selectedValue eq 0 } SELECTED {/if}>Order By Time</option>
  </SELECT>
  <INPUT type="submit" name="btnSubmit" value="Go !!"><BR><BR>
  <HR>
<TABLE border=1 cellSpacing="0" cellPadding="2" width="100%">

<TR>
  <TD colspan="7"><input type="submit" name="btnChecked" value="Done!!" onClick="setValue();"></TD>
  <TD colspan="3"><input type="checkbox" onclick="chkAll(document.form1.checkGroup,this)" id="checkGroup" name="checkAll1" />Check / Uncheck</TD>
</TR>

<TR bgcolor="#aaeeee">
  <TH>&nbsp;</TH>
  <TH>Id</TH>
  <TH>Name</TH>
  <TH>Status</TH>
  <TH>Date</TH>
  <TH>Item Name-Expiry Date</TH>
  <TH>Qty</TH>
  <TH>Price</TH>
  <TH>Amount</TH>
  <TH>Vendor</TH>
  </B>
</TR>
  {if $message neq "No Data Found"}
  {assign var="counter" value=0}
  {section name=sec1 loop=$num}
    {if $clientChange[sec1] eq 1}
      {if $Removetr eq 1}
      <TR bgcolor="#d0d0d0">
       </TR>
      {else}
        <TR bgcolor="#d0d0d0">
         <TH colspan=4>Buy Qty Total:{$totalBuyForComparision[$counter]}</TH>
         <TH colspan=3>Sell Qty Total:{$totalSellForComparision[$counter]}</TH>
         <TH colspan=3>Net Qty: {math equation="(x - y)" x=$totalBuyForComparision[$counter] y=$totalSellForComparision[$counter]}</TH>
       </TR>
      {/if}
    {/if}
    {if $buySellSelected[sec1] eq 'Buy'}
      <TR style="color:blue">
    {else}
      <TR style="color:red">
    {/if}
    {if $standing[sec1] eq -1}  
      <TR bgcolor="#FFFFB0">
    {/if}
    {if $standing[sec1] eq 1}  
      <TR bgcolor="#FFAA55">
    {/if}
      <TD>&nbsp;</TD>
      <TD>{$clientIdSelected[sec1]}</TD>
      <TD>{$clientNameSelected[sec1]} ( {$clientId2Selected[sec1]} : {$clientName2Selected[sec1]} )</TD>
      <TD>{$buySellSelected[sec1]}</TD>
      <TD>{$tradeDateSelected[sec1]}</TD>
      <TD>{$itemIdSelected[sec1]}-{$expiryDateSelected[sec1]}</TD>
      <TD>{$qtySelected[sec1]}</TD>
      <TD><input type="checkbox" name={$tradeIdSelected[sec1]} value="1" id="checkGroup" {$confirmed[sec1]}> {$priceSelected[sec1]}</TD>
      <TD>{$totalAmount[sec1]}</TD>
      <TD>
        <a href="./addTrade.php?exchange={$tradeExchange[sec1]}&tradeId{$tradeIdSelected}&goTo=tradeListForCrossChecking">Edit</a>
        &nbsp;
        <A onclick="return confirm('Are you sure?');" href="deleteTxt.php?tradeId={$trades[sec1].tradeId}">Delete</a>
      </TD>
    </TR>
  {/section}
  {if $Removetr eq 1}
      <TR bgcolor="#d0d0d0">
        
       </TR>
      {else}
      <TR bgcolor="#d0d0d0">
       <TH colspan=4>Buy Qty Total:{$totalBuyForComparision[$counter]}</TH>
       <TH colspan=3>Sell Qty Total:{$totalSellForComparision[$counter]}</TH>
       <TH colspan=3>Net Qty: {math equation="(x - y)" x=$totalBuyForComparision[$counter] y=$totalSellForComparision[$counter]}</TH>
     </TR>
     {/if}
    <TR bgcolor="#59ACAC">
       {if $totalBuyForComparision[$counter] eq $totalSellForComparision[$counter]}
         <TH colspan="10">Total Profit= {math equation="((x - y))" x=$totalSellAmount[$counter] y=$totalBuyAmount[$counter] format="%.2f"}</TH>
       {/if}
     </TR>
{else}
<TR><TH colspan="9">{$message}</TD><TR>
{/if}
<TR>
	<TD colspan="10">
		Buy Total : {$buyTotal}, Sell Total : {$sellTotal} ||  
		Buy Amount Total : {$grandTotalBuyAmount}, Sell Amount Total : {$grandTotalSellAmount} 
  </TD>
</TR>
<TR>
  <TD colspan="7"><input type="submit" name="btnChecked" value="Done!!" onClick="setValue();"></TD>
  <TD colspan="3"><input type="checkbox" onclick="chkAll(document.form1.checkGroup,this)" id="checkGroup" name="checkAll2" />Check / Uncheck</TD>
</TR>
</TABLE>
</FORM>
<SCRIPT name="javascript">
selectClient();
</SCRIPT>
</BODY>
</HTML>