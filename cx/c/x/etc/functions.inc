<?php
function mysqlToDDMMYY($dtToConvert)
{
  return substr($dtToConvert,8,2)."-".substr($dtToConvert,5,2)."-".substr($dtToConvert,2,2);
}

function standToDisplay($standValue)
{
  if($standValue == -1)
    return "Open";
  elseif($standValue == 1)
    return "Close";
  else
    return "&nbsp;";
}

function converInRs($varForConvert)
{
	if($varForConvert >= 0)
	  $bankRate = $_SESSION['profitBankRate'];
	else
	  $bankRate = $_SESSION['lossBankRate'];

  if(isset($bankRate) && $bankRate != 0)
    $inRsToDisplay = $varForConvert*$bankRate;
  else
    $inRsToDisplay = '';
    
  return $inRsToDisplay;
}

function array_search_recursive($needle, $haystack, $key_lookin="")
{
$path = NULL;
if (!empty($key_lookin) && array_key_exists($key_lookin, $haystack) && $needle === $haystack[$key_lookin]) {
$path[] = $key_lookin;
} else {
foreach($haystack as $key => $val) {
if (is_scalar($val) && $val === $needle && empty($key_lookin)) {
$path[] = $key;
break;
} 
elseif (is_array($val) && $path = array_search_recursive($needle, $val, $key_lookin)) {
array_unshift($path, $key);
break;
}
}
}
return $path;
}
/////////////////////////////////////////////////
function makecomma($input)
{
// This function is written by some anonymous person - I got it from Google
if(strlen($input)<=2)
{ return $input; }
$length=substr($input,0,strlen($input)-2);
$formatted_input = makecomma($length).",".substr($input,-2);
return $formatted_input;
}
function formatInIndianStyle($num){
// This is my function
$pos = strpos((string)$num, ".");
if ($pos === false) { $decimalpart="00";}
else { $decimalpart= substr($num, $pos+1, 2); $num = substr($num,0,$pos); }
if(strlen($num)>3 & strlen($num) <= 12){
$last3digits = substr($num, -3 );
$numexceptlastdigits = substr($num, 0, -3 );
$formatted = makecomma($numexceptlastdigits);
$stringtoreturn = $formatted.",".$last3digits.".".$decimalpart ;
}elseif(strlen($num)<=3){
$stringtoreturn = $num.".".$decimalpart ;
}elseif(strlen($num)>12){
$stringtoreturn = number_format($num, 2);
}
if(substr($stringtoreturn,0,2)=="-,"){$stringtoreturn = "-".substr($stringtoreturn,2 );}
return $stringtoreturn;
}
/////////////////////////////////////////////////
?>