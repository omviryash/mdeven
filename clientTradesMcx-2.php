<?php
session_start();
error_reporting(0);
ini_set("display_errors","Off");

if(!isset($_SESSION['user'])) {
    header("Location:login.php?goTo=clientTrades");
	exit;
}

if(!isset($_SESSION['toDate'])) {
  header("Location: selectDtSession.php?goTo=clientTrades");
  exit;
} else {
  include "./etc/om_config.inc";
  include "./etc/functions.inc";
  $smarty = new SmartyWWW();
  
  	if(isset($_GET['ex']) && trim($_GET['ex'])){
		$exch = trim($_GET['ex']);
	}
	else{
		$exch = 'mcx';
	}
  
  
  $message = "";
  
  ////Request parameters, if passed : transfer to proper variable :Start
  if(isset($_REQUEST['display']))
    $display = $_REQUEST['display'];
  else
    $display = 'trades';
  if(isset($_REQUEST['itemId']))
    $currentItemId = $_REQUEST['itemId'];
  else
    $currentItemId = "All";
	
	////Request parameters, if passed : transfer to proper variable :End
	
	////Group Selection for Combo : Start
	$groupIdSelected  = isset($_REQUEST['groupName']) ? $_REQUEST['groupName'] : "";
	$groupId          = array();
	$groupName        = array();
		
	$groupQuery = "SELECT groupId,groupName
	                 FROM groupList
	                ORDER BY groupName";
	$groupQueryResult = mysql_query($groupQuery);
	$groupCount             = 0;
    $groupId[$groupCount]   = 0;
    $groupName[$groupCount] = 'All';	
    $groupCount++;
	while($groupRow = mysql_fetch_array($groupQueryResult)) {
	  $groupId[$groupCount]     = $groupRow['groupId'];
	  $groupName[$groupCount]   = $groupRow['groupName'];
	  $groupCount++;
	}
	////Group Selection for Combo : End
	  
    //Client records :Start
    $clientIdSelected = isset($_REQUEST['clientId'])? $_REQUEST['clientId']:0;
	$clientIdValues = array();
	$clientIdOptions = array();
	$clientInfo      = array();
	$i = 0;
	$clientIdValues[0]  = 0;
	$clientIdOptions[0] = 'All';
	$i++;

	$clientQuery = "SELECT * FROM client WHERE 1 = 1 ";
	
	if(isset($_REQUEST['groupName']) && $_REQUEST['groupName'] != 'All')
	{
	  $clientQuery    .= " AND groupName = '".$groupIdSelected."'";
	}  
	$clientQuery .= " ORDER BY firstName, middleName, lastName";
	$clientResult = mysql_query($clientQuery);
	while($clientRow = mysql_fetch_array($clientResult)) {
		$clientIdValues[$i] = $clientRow['clientId'];
		$clientIdOptions[$i] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];

		$clientInfo[$clientRow['clientId']]['deposit'] = $clientRow['deposit'];
		$i++;
	}
	//Client records :End
	//Item records :Start
	$itemIdSelected = $currentItemId;
	$itemIdValues = array();
  $itemIdOptions = array();
  $itemCount = 0;
  $itemIdValues[0]  = "All";
  $itemIdOptions[0] = "All";
  $itemCount++;

  $itemRecords = array();
  $clientBrok  = array();
  $clientBrok1 = array();
  $clientBrok2 = array();
  $itemQuery = "SELECT * FROM item ";
  
  $itemQuery .= " WHERE exchange = '".($exch == "cmx"?"COMEX":"MCX")."'";
	
  $itemQuery .= " ORDER BY itemId";
  $itemResult = mysql_query($itemQuery);
  while($itemRow = mysql_fetch_array($itemResult))
  {
    $itemRecords[$itemRow['itemId']]['priceOn'] = $itemRow['priceOn'];
    $itemRecords[$itemRow['itemId']]['min']     = $itemRow['min'];
    $itemRecords[$itemRow['itemId']]['high']    = $itemRow['high'];
    $itemRecords[$itemRow['itemId']]['low']     = $itemRow['low'];
    
    //ClientBrok :Start
    for($i=0;$i<count($clientIdValues);$i++)
    {
      $clientBrok[$clientIdValues[$i]][$itemRow['itemId']]  = $itemRow['oneSideBrok'];
      $clientBrok1[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
      $clientBrok2[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
    }
    //ClientBrok :End
    
    $itemIdValues[$itemCount]  = $itemRow['itemId'];
    $itemIdOptions[$itemCount] = $itemRow['itemId'];
    $itemCount++;
  }
  //Item records :End
  //Override clientBrok from clientBrok table :Start
  $clientBrokQuery = "SELECT * FROM clientbrok ORDER BY clientId";
  $clientBrokResult = mysql_query($clientBrokQuery);
  while($clientBrokRow = mysql_fetch_array($clientBrokResult))
  {
    $clientBrok[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['oneSideBrok'];
    $clientBrok1[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['brok1'];
    $clientBrok2[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['brok2'];
  }
  //Override clientBrok from clientBrok table :Start
  
  //Expiry records :Start
  if(isset($_REQUEST['expiryDate']))
  {
    if(isset($_REQUEST['itemIdChanged']) && ($_REQUEST['itemIdChanged']==1 || $currentItemId=="All"))
      $expiryDateSelected = 0;
    else
      $expiryDateSelected = $_REQUEST['expiryDate'];
  }
  else
    $expiryDateSelected = 0;
    
  $expiryDateValues = array();
  $expiryDateOptions = array();
  $i = 0;
  $expiryDateValues[0]  = 0;
  $expiryDateOptions[0] = 'All';
  $i++;

  if($currentItemId!="All")
  {
    //$expiryQuery = "SELECT * FROM expiry ORDER BY itemId, expiryDate";
	
	if($exch == 'mcx'){
		$expiryQuery = "SELECT * FROM expiry where exchange='MCX' ORDER BY itemId, expiryDate";
	}
	else{
		$expiryQuery = "SELECT * FROM expiry where exchange='COMEX' ORDER BY itemId, expiryDate";
	}
		
    $expiryResult = mysql_query($expiryQuery);
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      if($expiryRow['itemId'] == $currentItemId)
      {
        $expiryDateValues[$i]  = $expiryRow['expiryDate'];
        $expiryDateOptions[$i] = $expiryRow['expiryDate'];
        $i++;
      }
    }
  }
  //Expiry records :End

  $trades = array();
  $expiryDetails = array();
  //$expiryQuery = "SELECT * FROM expiry ORDER BY itemId, str_to_date(expiryDate,'%d%b%Y')";
  
  if($exch == 'mcx'){
		$expiryQuery = "SELECT * FROM expiry where exchange = 'MCX' ORDER BY itemId, str_to_date(expiryDate,'%d%b%Y')";
	}
	else{
		$expiryQuery = "SELECT * FROM expiry where exchange = 'COMEX' ORDER BY itemId, str_to_date(expiryDate,'%d%b%Y')";
	}
	
    $expiryResult = mysql_query($expiryQuery);
	$inc = 0;
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
        $expiryDetails[$inc]['id']  = $expiryRow['expiryId'];
		$expiryDetails[$inc]['itemId']  = $expiryRow['itemId'];
		$expiryDetails[$inc]['expiryDate']  = $expiryRow['expiryDate'];
		$inc++;
	}
  $prevClientId = 0;
  $prevItemId = '';
  $prevExpiryDate = '';
  $valuesForGrossLine = array();
  $wholeItemArr       = array();
  $wholeItemArrCount  = -1;  //-1, because we do ++ when we store 0

  $wholeBuyQty        = 0;
  $wholeTotBuyAmount  = 0;
  $wholeBuyRash       = 0;
  $wholeSellQty       = 0;
  $wholeTotSellAmount = 0;
  $wholeSellRash      = 0;
  $wholeProfitLoss    = 0;
  $wholeOneSideBrok   = 0;
  $wholeNetProfitLoss = 0;
  $wholeNetLossOnly   = 0;
  $wholeNetProfitOnly = 0;

	$wholeBrok1 = 0;
  $wholeBrok2 = 0;

  $valuesForGrossLine['openBuyQty'] = 0;
  $valuesForGrossLine['openSellQty'] = 0;
  $valuesForGrossLine['totBuyQty'] = 0;
  $valuesForGrossLine['totSellQty'] = 0;
  $valuesForGrossLine['totBuyAmount'] = 0;
  $valuesForGrossLine['totSellAmount'] = 0;
  
  $i = 0;
  

  if($exch == 'mcx'){
	$tradesQuery = "SELECT * FROM tradetxt";
  }
  else{
	$tradesQuery = "SELECT * FROM tradetxtcx";
  }
/////////////////////////////////////////////Where Condition :Start
  $whereGiven = false;
  $whereCondition = "";
  if(isset($_REQUEST['clientId']) && $_REQUEST['clientId']!=0)
  {
    $whereCondition .= " WHERE clientId = ".$_REQUEST['clientId'];
    $whereGiven = true;
  }
  if($whereGiven == false)
  {
    $whereCondition .= " WHERE 1 = 1 ";
    $whereGiven = true;
  }
  if(isset($_REQUEST['groupName']) && $_REQUEST['groupName'] != 'All')
  {
  	$whereCondition .= " AND clientId IN (SELECT clientId 
  	                                     FROM client 
  	                                    WHERE groupName = '".$_REQUEST['groupName']."')";
  }
  if(isset($_REQUEST['buySellOnly']) && $_REQUEST['buySellOnly'] != 'All')
  	$whereCondition .= " AND buySell LIKE '".$_REQUEST['buySellOnly']."'";
  
  if($currentItemId!="All")
  {
    if($whereGiven)
      $whereCondition .= " AND   itemId LIKE '".$currentItemId."'";
    else
      $whereCondition .= " WHERE itemId LIKE '".$currentItemId."'";
    $whereGiven = true;
  }
  if(isset($_REQUEST['expiryDate']) && $_REQUEST['expiryDate']!='0' && $_REQUEST['itemIdChanged']!=1 && $currentItemId!="All")
  {
    if($whereGiven)
      $whereCondition .= " AND   expiryDate LIKE '".$_REQUEST['expiryDate']."'";
    else
      $whereCondition .= " WHERE expiryDate LIKE '".$_REQUEST['expiryDate']."'";
    $whereGiven = true;
  }

  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
    if($whereGiven)
     $whereCondition .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
    else
    {
      $whereCondition .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
      $whereGiven = true;
    }
  }

  if($display == 'openStand')
  {
    if($whereGiven)
     $whereCondition .= " AND standing = -1";
    else
    {
      $whereCondition .= " WHERE standing = -1";
      $whereGiven = true;
    }
  }
  if($display == 'closeStand')
  {
    if($whereGiven)
     $whereCondition .= " AND standing = 1";
    else
    {
      $whereCondition .= " WHERE standing = 1";
      $whereGiven = true;
    }
  }
  if($display == 'openCloseStand')
  {
    if($whereGiven)
     $whereCondition .= " AND (standing = -1 OR standing = 1)";
    else
    {
      $whereCondition .= " WHERE (standing = -1 OR standing = 1)";
      $whereGiven = true;
    }
  }
  
  if($display == 'noInnerStand')
  {
    if($whereGiven)
      $whereCondition .= " AND NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
    else
    {
      $whereCondition .= " WHERE NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
      $whereGiven = true;
    }
  }
  
  if(isset($_REQUEST['exchange']) && $_REQUEST['exchange'] != "All")
    $whereCondition .= " AND itemId IN (SELECT itemId FROM item
                                      WHERE exchange LIKE '".$_REQUEST['exchange']."') ";

  $tradesQuery .= $whereCondition;
/////////////////////////////////////////////Where Condition :End
  applyConfirm();

  $tradesQuery .= " ORDER BY firstName, middleName, lastName, itemId ASC, expiryDate, 
                     tradeDate ASC, standing, tradeTime, tradeId";
  $tradesResult = mysql_query($tradesQuery);
  if(mysql_num_rows($tradesResult) == 0)
    $message = "No records!";
  else
  {
    while($tradesRow = mysql_fetch_array($tradesResult))
    {
      $trades[$i]['tradeId']         = $tradesRow['tradeId'];
      $trades[$i]['confirmed']       = $tradesRow['confirmed'];
      $trades[$i]['dispGross']       = 0;
      $trades[$i]['dispClientWhole'] = 0;
      $trades[$i]['highLowConf']     = $tradesRow['highLowConf'];
      
      if($i==0)
      { $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;  $valuesForGrossLine['clientPrevBrok1'] = 0;  $valuesForGrossLine['clientPrevBrok2'] = 0;  }
  
      //For Gross line :Start //Gross section comes initially in while loop, but it is used after displaying trades ... it is here because when client or item or expiry change, first we store data to display gross for previous matter
      if($tradesRow['clientId'] != $prevClientId || $tradesRow['itemId'] != $prevItemId || $tradesRow['expiryDate'] != $prevExpiryDate)
      {
        if($prevClientId != 0)
        {
          $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
          $valuesForGrossLine['clientPrevProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'];
          $valuesForGrossLine['clientPrevBrok']       = $trades[$i-1]['clientTotBrok'];
          $valuesForGrossLine['clientPrevBrok1']      = $trades[$i-1]['clientTotBrok1'];
          $valuesForGrossLine['clientPrevBrok2']      = $trades[$i-1]['clientTotBrok2'];
          
          $valuesForGrossLine['openBuyQty'] = 0;
          $valuesForGrossLine['openSellQty'] = 0;
          $valuesForGrossLine['totBuyQty']  = 0;
          $valuesForGrossLine['totSellQty'] = 0;
          $valuesForGrossLine['totBuyAmount'] = 0;
          $valuesForGrossLine['totSellAmount'] = 0;
          
          if($tradesRow['clientId'] != $prevClientId)
          {
            $trades[$i-1]['dispClientWhole'] = 1;
            $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;  $valuesForGrossLine['clientPrevBrok1'] = 0;  $valuesForGrossLine['clientPrevBrok2'] = 0;
          }
        }
      }
      //For Gross line :End
      //priceOn and min are here because we need to assign value after gross line, and also, at first time : we take first priceOn and min and then we use it in gross line
      $priceOn = $itemRecords[$tradesRow['itemId']]['priceOn'];
      $valuesForGrossLine['min'] = $itemRecords[$tradesRow['itemId']]['min'];
  
      //We take first oneSideBrok and then use in Gross line
      $valuesForGrossLine['oneSideBrok'] = $clientBrok[$tradesRow['clientId']][$tradesRow['itemId']];
      $valuesForGrossLine['brok1'] = $clientBrok1[$tradesRow['clientId']][$tradesRow['itemId']];
      $valuesForGrossLine['brok2'] = $clientBrok2[$tradesRow['clientId']][$tradesRow['itemId']];
      
      $trades[$i]['clientId']     = $tradesRow['clientId'];
      $trades[$i]['prevClientId'] = $prevClientId;
      $prevClientId               = $tradesRow['clientId'];
      $trades[$i]['itemId']       = $tradesRow['itemId'];
      $trades[$i]['prevItemId']   = $prevItemId;
      $prevItemId                 = $tradesRow['itemId'];
      $trades[$i]['expiryDate']   = $tradesRow['expiryDate'];
      $trades[$i]['prevExpiryDate']   = $prevExpiryDate;
      $prevExpiryDate             = $tradesRow['expiryDate'];
  
      $trades[$i]['clientName'] = trim($tradesRow['firstName']." ".$tradesRow['middleName']." ".$tradesRow['lastName']);
      $trades[$i]['clientDeposit'] = $clientInfo[$tradesRow['clientId']]['deposit'];
      $trades[$i]['vendor']    = $tradesRow['vendor'];
      $trades[$i]['tradeDate'] = mysqlToDDMMYY($tradesRow['tradeDate']);
      $trades[$i]['tradeTime'] = $tradesRow['tradeTime'];
      $trades[$i]['standing']  = standToDisplay($tradesRow['standing']);
      $trades[$i]['itemIdExpiry'] = $tradesRow['itemId']."-".substr($tradesRow['expiryDate'],2,3);
      $trades[$i]['buySell']   = $tradesRow['buySell'];
  
      $trades[$i]['tradeRefNo']  = $tradesRow['tradeRefNo'];
      $trades[$i]['userRemarks'] = $tradesRow['userRemarks'];
      $trades[$i]['ownClient']   = $tradesRow['ownClient'];
  
      if($tradesRow['buySell'] == 'Buy')
      {
        $trades[$i]['fontColor'] = "blue";
        $trades[$i]['buyQty']    = $tradesRow['qty'];
        $trades[$i]['price']     = $tradesRow['price'];
        $trades[$i]['sellQty']   = '&nbsp;';
        $trades[$i]['sellPrice'] = '&nbsp;';
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openBuyQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totBuyQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totBuyAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      else
      {
        $trades[$i]['fontColor'] = "red";
        $trades[$i]['buyQty'] = '&nbsp;';
        $trades[$i]['price'] = '&nbsp;';
        $trades[$i]['sellQty'] = $tradesRow['qty'];
        $trades[$i]['sellPrice'] = $tradesRow['price'];
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openSellQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totSellQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totSellAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      $i++;
    }
    $totTrades = $i;
    
    $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
    $trades[$i-1]['dispClientWhole'] = 1;

    //formatInIndianStyle :Start
    for($i=0;$i<$totTrades;$i++)
    {
      $trades[$i]['totBuyAmount']  = formatInIndianStyle(isset($trades[$i]['totBuyAmount'])?$trades[$i]['totBuyAmount']:0);
      $trades[$i]['totSellAmount'] = formatInIndianStyle(isset($trades[$i]['totSellAmount'])?$trades[$i]['totSellAmount']:0);
      $trades[$i]['buyRash']       = number_format(isset($trades[$i]['buyRash'])?$trades[$i]['buyRash']:0,2,'.','');
      $trades[$i]['sellRash']      = number_format(isset($trades[$i]['sellRash'])?$trades[$i]['sellRash']:0,2,'.','');
      $trades[$i]['profitLoss']    = formatInIndianStyle(isset($trades[$i]['profitLoss'])?$trades[$i]['profitLoss']:0);
      $trades[$i]['oneSideBrok']   = formatInIndianStyle(isset($trades[$i]['oneSideBrok'])?$trades[$i]['oneSideBrok']:0);
      $trades[$i]['brok1']         = formatInIndianStyle(isset($trades[$i]['brok1'])?$trades[$i]['brok1']:0);
      $trades[$i]['brok2']         = formatInIndianStyle(isset($trades[$i]['brok2'])?$trades[$i]['brok2']:0);
      $trades[$i]['netProfitLossNotFormatted'] = isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0;
      $trades[$i]['netProfitLoss'] = formatInIndianStyle(isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0);
      
      $trades[$i]['clientTotProfitLoss'] = formatInIndianStyle(isset($trades[$i]['clientTotProfitLoss'])?$trades[$i]['clientTotProfitLoss']:0);
      $trades[$i]['clientTotBrok']       = formatInIndianStyle(isset($trades[$i]['clientTotBrok'])?$trades[$i]['clientTotBrok']:0);
      $trades[$i]['clientTotBrok1']       = formatInIndianStyle(isset($trades[$i]['clientTotBrok1'])?$trades[$i]['clientTotBrok1']:0);
      $trades[$i]['clientTotBrok2']       = formatInIndianStyle(isset($trades[$i]['clientTotBrok2'])?$trades[$i]['clientTotBrok2']:0);
      $trades[$i]['clientTotNetProfitLossNoFormat'] = isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0;
      $trades[$i]['clientTotNetProfitLoss'] = formatInIndianStyle(isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0);
    }
    for($i=0;$i<=$wholeItemArrCount;$i++)
    {
  //////////////For whole total :Start
      $wholeBuyQty        += $wholeItemArr[$i]['buyQty'];
      $wholeTotBuyAmount  += $wholeItemArr[$i]['totBuyAmount'];
      $wholeSellQty       += $wholeItemArr[$i]['sellQty'];
      $wholeTotSellAmount += $wholeItemArr[$i]['totSellAmount'];
      $wholeProfitLoss    += $wholeItemArr[$i]['profitLoss'];
      $wholeOneSideBrok   += $wholeItemArr[$i]['oneSideBrok'];
      $wholeBrok1         += $wholeItemArr[$i]['brok1'];
      $wholeBrok2         += $wholeItemArr[$i]['brok2'];
      $wholeNetProfitLoss += $wholeItemArr[$i]['netProfitLoss'];
      if($wholeItemArr[$i]['netProfitLoss'] >= 0) $wholeNetProfitOnly += $wholeItemArr[$i]['netProfitLoss'];
      else $wholeNetLossOnly += $wholeItemArr[$i]['netProfitLoss'];

  //////////////For whole total :End
  //////////////For number_format :Start
      $wholeItemArr[$i]['totBuyAmount']  = formatInIndianStyle($wholeItemArr[$i]['totBuyAmount']);
      $wholeItemArr[$i]['totSellAmount'] = formatInIndianStyle($wholeItemArr[$i]['totSellAmount']);
      $wholeItemArr[$i]['buyRash']       = number_format($wholeItemArr[$i]['buyRash'],2,'.','');
      $wholeItemArr[$i]['sellRash']      = number_format($wholeItemArr[$i]['sellRash'],2,'.','');
      $wholeItemArr[$i]['profitLoss']    = formatInIndianStyle($wholeItemArr[$i]['profitLoss']);
      $wholeItemArr[$i]['oneSideBrok']   = formatInIndianStyle($wholeItemArr[$i]['oneSideBrok']);
      $wholeItemArr[$i]['brok1']         = formatInIndianStyle($wholeItemArr[$i]['brok1']);
      $wholeItemArr[$i]['brok2']         = formatInIndianStyle($wholeItemArr[$i]['brok2']);
      $wholeItemArr[$i]['netProfitLoss'] = formatInIndianStyle($wholeItemArr[$i]['netProfitLoss']);
  //////////////For number_format :End
    }
    $wholeBuyRash  = ($wholeBuyQty!=0)?($wholeTotBuyAmount/$wholeBuyQty):0;
    $wholeSellRash = ($wholeSellQty!=0)?($wholeTotSellAmount/$wholeSellQty):0;
  }
  //formatInIndianStyle :End
  
  ///////////////// buySellOnly :Start
  $buySellOnlySelected = "All";
  if(isset($_REQUEST['buySellOnly']))
  {
  	$buySellOnlySelected = $_REQUEST['buySellOnly'];
  	
    if($_REQUEST['buySellOnly'] == 'All')
    {
    	$buySellOnlyAll  = "SELECTED";
    	$buySellOnlyBuy  = "";
    	$buySellOnlySell = "";
    }
    elseif($_REQUEST['buySellOnly'] == 'Buy')
    {
    	$buySellOnlyAll  = "";
    	$buySellOnlyBuy  = "SELECTED";
    	$buySellOnlySell = "";
    }
    else
    {
    	$buySellOnlyAll  = "";
    	$buySellOnlyBuy  = "";
    	$buySellOnlySell = "SELECTED";
    }
  }
  else
  {
  	$buySellOnlyAll  = "SELECTED";
  	$buySellOnlyBuy  = "";
  	$buySellOnlySell = "";
  }
  ///////////////// buySellOnly :End

  ///////////////// exchangeOnly :Start
  $exchangeSelected = "MCX";
  if(isset($_REQUEST['exchange']))
  {
  	$exchangeSelected = $_REQUEST['exchange'];
  	
    if($_REQUEST['exchange'] == 'All')
    {
    	$exchangeOnlyAll  = "SELECTED";
    	$exchangeOnlyMCX  = "";
    	$exchangeOnlyComex = "";
    }
    elseif($_REQUEST['exchange'] == 'MCX')
    {
    	$exchangeOnlyAll  = "";
    	$exchangeOnlyMCX  = "SELECTED";
    	$exchangeOnlyComex = "";
    }
    else
    {
    	$exchangeOnlyAll  = "";
    	$exchangeOnlyMCX  = "";
    	$exchangeOnlyComex = "SELECTED";
    }
  }
  else
  {
  	$exchangeOnlyAll  = "SELECTED";
  	$exchangeOnlyMCX  = "";
  	$exchangeOnlyComex = "";
  }
  ///////////////// exchangeOnly :End
  ///////////////// Client Overwrite : Start
  $clientOverwriteQuery  = "SELECT * FROM client WHERE clientId IN ( ";
///////////////////////////////////////////////////

	//$tradesQueryForClient = "SELECT clientId FROM tradetxt WHERE 1 = 1 ";
	
	if($exch == 'mcx'){
		$tradesQueryForClient = "SELECT clientId FROM tradetxt WHERE 1 = 1 ";
  	}
  	else{
		$tradesQueryForClient = "SELECT clientId FROM tradetxtcx WHERE 1 = 1 ";
  	}  
  
  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
     $tradesQueryForClient .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
  }
  $clientOverwriteQuery  .= $tradesQueryForClient." ) ";
///////////////////////////////////////////////////

  $clientOverwriteQuery .= " ORDER BY firstName, middleName, lastName";
  $clientOverwriteResult = mysql_query($clientOverwriteQuery);
  $clientIdValues = NULL;
  $clientIdOptions = NULL;
  $clientIdValues = array();
  $clientIdOptions = array();
  $i = 0;
  $clientIdValues[0]  = 0;
  $clientIdOptions[0] = 'All';
  $i++;
  while($clientOverwriteRow = mysql_fetch_array($clientOverwriteResult))
  {
    $clientIdValues[$i] = $clientOverwriteRow['clientId'];
    $clientIdOptions[$i] = $clientOverwriteRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
    $i++;
  }
  ///////////////// Client Overwrite :End
  $smarty->assign("expiryDetails",$expiryDetails);
  $smarty->assign("groupIdSelected",$groupIdSelected);
  $smarty->assign("groupId",$groupId);
  $smarty->assign("groupName",$groupName);
  $smarty->assign("buySellOnlyAll",$buySellOnlyAll);
  $smarty->assign("buySellOnlyBuy",$buySellOnlyBuy);
  $smarty->assign("buySellOnlySell",$buySellOnlySell);
  $smarty->assign("buySellOnlySelected",$buySellOnlySelected);
  
  $smarty->assign("exchangeOnlyAll",$exchangeOnlyAll);
  $smarty->assign("exchangeOnlyMCX",$exchangeOnlyMCX);
  $smarty->assign("exchangeOnlyComex",$exchangeOnlyComex);
  $smarty->assign("exchangeSelected",$exchangeSelected);

  $smarty->assign("PHP_SELF", $_SERVER['PHP_SELF']);
  $smarty->assign("goTo", "clientTrades");
  $smarty->assign("display", $display);
  $smarty->assign("message", $message);
  $smarty->assign("clientIdSelected", $clientIdSelected);
  $smarty->assign("clientIdValues",   $clientIdValues);
  $smarty->assign("clientIdOptions",  $clientIdOptions);
  $smarty->assign("itemIdSelected",   $itemIdSelected);
  $smarty->assign("itemIdValues",     $itemIdValues);
  $smarty->assign("itemIdOptions",    $itemIdOptions);
  $smarty->assign("expiryDateSelected", $expiryDateSelected);
  $smarty->assign("expiryDateValues",   $expiryDateValues);
  $smarty->assign("expiryDateOptions",  $expiryDateOptions);

  $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
  $smarty->assign("profitLossDate", substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],0,4));
  
  $smarty->assign("trades",           $trades);
  $smarty->assign("wholeItemArr",     $wholeItemArr);

  $smarty->assign("wholeBuyQty",        $wholeBuyQty);
  $smarty->assign("wholeTotBuyAmount",  formatInIndianStyle($wholeTotBuyAmount));
  $smarty->assign("wholeBuyRash",       number_format($wholeBuyRash,2,'.',''));
  $smarty->assign("wholeSellQty",       $wholeSellQty);
  $smarty->assign("wholeTotSellAmount", formatInIndianStyle($wholeTotSellAmount));
  $smarty->assign("wholeSellRash",      number_format($wholeSellRash,2,'.',''));
  $smarty->assign("wholeProfitLoss",    formatInIndianStyle($wholeProfitLoss));
  $smarty->assign("wholeOneSideBrok",   formatInIndianStyle($wholeOneSideBrok));
  $smarty->assign("wholeBrok1",   formatInIndianStyle($wholeBrok1));
  $smarty->assign("wholeBrok2",   formatInIndianStyle($wholeBrok2));
  $smarty->assign("wholeNetProfitLoss", formatInIndianStyle($wholeNetProfitLoss));
  $smarty->assign("wholeNetProfitOnly", formatInIndianStyle($wholeNetProfitOnly));
  $smarty->assign("wholeNetLossOnly",   formatInIndianStyle($wholeNetLossOnly));
  $smarty->assign("isFrom",'MCX');
  
   $smarty->assign("ex",$exch);

/////////////////Use tpl as per 'display' parameter :Start
  if($display == 'trades')
    $smarty->display("clientTrades-2.tpl");
  elseif($display == 'detailed')
    $smarty->display("clientTrades.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'gross')
    $smarty->display("clientGross.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("clientItemWiseGross.tpl");
  elseif($display == 'itemPending')
    $smarty->display("clientItemPending.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("clientStand.tpl");
  elseif($display == 'PLToAccount')
    $smarty->display("clientPLToAccount.tpl");
  elseif($display == 'tradesPrint')
    $smarty->display("clientTradesPrint.tpl");
  elseif($display == 'grossPrint')
    $smarty->display("clientGrossPrint.tpl");
  else
    $smarty->display("clientTrades.tpl");
/////////////////Use tpl as per 'display' parameter :End
}
///////////////////////////////
function updateForGrossLine($trades, $i, $valuesForGrossLine)
{
  global $wholeItemArr, $wholeItemArrCount;
  $trades[$i-1]['dispGross']     = 1;
  $trades[$i-1]['totBuyQty']     = $valuesForGrossLine['totBuyQty'];
  $trades[$i-1]['totSellQty']    = $valuesForGrossLine['totSellQty'];
  $trades[$i-1]['totBuyAmount']  = $valuesForGrossLine['totBuyAmount'];
  $trades[$i-1]['totSellAmount'] = $valuesForGrossLine['totSellAmount'];
  $trades[$i-1]['buyRash']       = ($valuesForGrossLine['totBuyQty']!=0)?($valuesForGrossLine['totBuyAmount']/$valuesForGrossLine['totBuyQty']):0;
  $trades[$i-1]['sellRash']      = ($valuesForGrossLine['totSellQty']!=0)?($valuesForGrossLine['totSellAmount']/$valuesForGrossLine['totSellQty']):0;
  
  if($trades[$i-1]['totBuyQty'] == $trades[$i-1]['totSellQty'])
  {
    $trades[$i-1]['profitLoss']   = $valuesForGrossLine['totSellAmount'] - $valuesForGrossLine['totBuyAmount'];
    
    //We subtract openBuyQty and openSellQty... but actually there will be only 1 open qty : buy or sell ...so result will be ok
    $trades[$i-1]['oneSideBrok'] 
     = $valuesForGrossLine['oneSideBrok']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];
    
    $trades[$i-1]['brok1'] 
     = $valuesForGrossLine['brok1']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];

    $trades[$i-1]['brok2'] 
     = $valuesForGrossLine['brok2']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];
       
    $trades[$i-1]['netProfitLoss']= $trades[$i-1]['profitLoss'] - $trades[$i-1]['oneSideBrok'];
	$trades[$i-1]['netProfitLoss_N']= $trades[$i-1]['profitLoss'];
  }
  else
  {
    $trades[$i-1]['profitLoss']   = 0;
    $trades[$i-1]['oneSideBrok']  = 0;
    $trades[$i-1]['brok1']  = 0;
    $trades[$i-1]['brok2']  = 0;
    $trades[$i-1]['netProfitLoss']= 0;
	$trades[$i-1]['netProfitLoss_N']=0;
  }
  
  $trades[$i-1]['clientTotProfitLoss'] = $valuesForGrossLine['clientPrevProfitLoss'] + $trades[$i-1]['profitLoss'];
  $trades[$i-1]['clientTotBrok']       = $valuesForGrossLine['clientPrevBrok'] + $trades[$i-1]['oneSideBrok'];
  $trades[$i-1]['clientTotBrok1']       = $valuesForGrossLine['clientPrevBrok1'] + $trades[$i-1]['brok1'];
  $trades[$i-1]['clientTotBrok2']       = $valuesForGrossLine['clientPrevBrok2'] + $trades[$i-1]['brok2'];
  $trades[$i-1]['clientTotNetProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'] - $trades[$i-1]['clientTotBrok'];

  if(!array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr))
  {
    $wholeItemArrCount++;
    $wholeItemArr[$wholeItemArrCount]['itemIdExpiry']  = $trades[$i-1]['itemIdExpiry'];
	$wholeItemArr[$wholeItemArrCount]['itemId']  = $trades[$i-1]['itemId'];
	$wholeItemArr[$wholeItemArrCount]['expiryDate']  = $trades[$i-1]['expiryDate'];
    $wholeItemArr[$wholeItemArrCount]['buyQty']        = 0;
    $wholeItemArr[$wholeItemArrCount]['sellQty']       = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmount'] = 0;
    $wholeItemArr[$wholeItemArrCount]['profitLoss']    = 0;
    $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   = 0;
    $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] = 0;
    $wholeItemArr[$wholeItemArrCount]['brok1']         = 0;
    $wholeItemArr[$wholeItemArrCount]['brok2']         = 0;
    
    $elementNoToUse = $wholeItemArrCount;
  }
  else
  {
    $foundArray = array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr);
    $elementNoToUse = $foundArray[0];
  }
  
  $wholeItemArr[$elementNoToUse]['buyQty']        += $trades[$i-1]['totBuyQty'];
  $wholeItemArr[$elementNoToUse]['sellQty']       += $trades[$i-1]['totSellQty'];
  $wholeItemArr[$elementNoToUse]['totBuyAmount']  += $trades[$i-1]['totBuyAmount'];
  $wholeItemArr[$elementNoToUse]['totSellAmount'] += $trades[$i-1]['totSellAmount'];
  $wholeItemArr[$elementNoToUse]['buyRash']       = ($wholeItemArr[$wholeItemArrCount]['buyQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totBuyAmount']/$wholeItemArr[$wholeItemArrCount]['buyQty']):0;
  $wholeItemArr[$elementNoToUse]['sellRash']      = ($wholeItemArr[$wholeItemArrCount]['sellQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totSellAmount']/$wholeItemArr[$wholeItemArrCount]['sellQty']):0;
  $wholeItemArr[$elementNoToUse]['profitLoss']    += $trades[$i-1]['profitLoss'];
  $wholeItemArr[$elementNoToUse]['oneSideBrok']   += $trades[$i-1]['oneSideBrok'];
  $wholeItemArr[$elementNoToUse]['netProfitLoss'] += $trades[$i-1]['netProfitLoss_N'];
  $wholeItemArr[$elementNoToUse]['brok1']         += $trades[$i-1]['brok1'];
  $wholeItemArr[$elementNoToUse]['brok2']         += $trades[$i-1]['brok2'];

  return $trades;
}
  $comboClientId = isset($_REQUEST['clientId'])?$_REQUEST['clientId']:0;
  include "./accTransList3.php";
  
function applyConfirm()
{
	global $whereCondition;
  if(isset($_REQUEST['confirmBtn']))
  {
  	//$updateQuery  = "UPDATE tradetxt SET confirmed = 0";
	
	if($exch == 'mcx'){
		$updateQuery  = "UPDATE tradetxt SET confirmed = 0";
  	}
  	else{
		$updateQuery  = "UPDATE tradetxtcx SET confirmed = 0";
  	} 
	
  	$updateQuery .= $whereCondition;
  	
  	mysql_query($updateQuery);
  	
  	if(isset($_REQUEST['confirmed']))
  	{
  		$confirmedArray = implode(",", $_REQUEST['confirmed']);
	  	
		//$updateQuery  = "UPDATE tradetxt SET confirmed = 1 WHERE tradeId IN (".$confirmedArray.")";
		
		if($exch == 'mcx'){
			$updateQuery  = "UPDATE tradetxt SET confirmed = 1 WHERE tradeId IN (".$confirmedArray.")";
		}
		else{
			$updateQuery  = "UPDATE tradetxtcx SET confirmed = 1 WHERE tradeId IN (".$confirmedArray.")";
		}
  	
    	mysql_query($updateQuery);
    }
    
    $headerText  = isset($_REQUEST['display'])        ? "display=".$_REQUEST['display'] : "";
    $headerText .= isset($_REQUEST['itemIdChanged'])  ? "&itemIdChanged=".$_REQUEST['itemIdChanged'] : "";
    $headerText .= isset($_REQUEST['clientId'])       ? "&clientId=".$_REQUEST['clientId'] : "";
    $headerText .= isset($_REQUEST['itemId'])         ? "&itemId=".$_REQUEST['itemId'] : "";
    $headerText .= isset($_REQUEST['buySellOnly'])    ? "&buySellOnly=".$_REQUEST['buySellOnly'] : "";
    $headerText .= isset($_REQUEST['groupName'])      ? "&groupName=".$_REQUEST['groupName'] : "";
    $headerText .= isset($_REQUEST['expiryDate'])     ? "&expiryDate=".$_REQUEST['expiryDate'] : "";
    $headerText .= isset($_REQUEST['exchange'])       ? "&exchange=".$_REQUEST['exchange'] : "";
    header("Location: ./clientTrades.php?".$headerText);
    exit();
  }
}

mysql_close();
?>