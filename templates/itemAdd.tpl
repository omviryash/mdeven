<HTML>
<HEAD><TITLE>Brokerage settings</TITLE>
{literal}
<script type="text/javascript">
function alphanumeric() 
{
  if( (event.keyCode >= 47 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) 
       || (event.keyCode >= 97 && event.keyCode <= 122) || (event.keyCode == 95) ) 
  {
    return true;
  }
  else
  {
    alert("Please Enter Only Alphanumeric");
    return false;
  }
    
}
</script>
{/literal}
</HEAD>
<BODY bgColor="#FFCEE7">
<CENTER>
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <B><A href="./index.php">Home</A></B><BR><BR>
  <B><A href="brokerage.php">Item List</A></B><BR><BR>
  <TABLE border="1" cellSpacing="0" cellPadding="2">
  <TR>
    <TD colspan="8" align="center">
      <select name="exchange">
      {section name=sec loop=$exchangeId}
        {html_options values=$exchangeId[sec] output=$exchange[sec]}
      {/section}
      </select>
   </TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">Item</FONT></TD>
    <TD align="center"><INPUT type='text' name='item' size='15' onKeyPress="return alphanumeric();"></TD>
        <INPUT type='hidden' name='brok' value='0' size='15'>
        <INPUT type='hidden' name='brok2' value='0' size='15'>
        <INPUT type='hidden' name='priceOn'value='1' size='15'>
      
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">OneSideBrok </FONT></TD>
    <TD align="center"><INPUT type='text' name='oneSideBrok' size='15'></TD>
    <TD align="center"><B>For example, for Gold : </B></TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">Minimum </FONT></TD>
    <TD align="center"><INPUT type='text' name='min' size='15'></TD>
    <TD align="center"><FONT color="red">100</FONT></TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta"> Price Range Start </FONT></TD>
    <TD align="center"><INPUT type='text' name='rangeStart' size='15' value="0"></TD>
    <TD align="center"><FONT color="red">5800</FONT></TD>
  </TR>
  <TR>
    <TD align="center"> <FONT color="DarkMagenta">Price Range End </FONT></TD>
    <TD align="center"><INPUT type='text' name='rangeEnd' size='15' value="0"></TD>
    <TD align="center"><FONT color="red">6999</FONT></TD>
  </TR>
   <TR>
    <TD align="center"> <FONT color="DarkMagenta">Multiply</FONT></TD>
    <TD align="center"><INPUT type='text' name='multiply' size='15' value='1'></TD>
    <TD align="center"><FONT color="red">&nbsp;</FONT></TD>
  </TR>
  <TR>
    <TD align="center"><INPUT type="submit" name="submitBtn" value="Submit!"></TD>
    <TD align="center"><INPUT type="reset" value="Reset"></TD>
    <TD align="center"></TD>
  </TR>
  </TABLE>
 <SCRIPT language="javascript">document.form1.item.focus();</SCRIPT>
  </FORM>
</CENTER>
</BODY>
</HTML>